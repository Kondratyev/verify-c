(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)

(defun inc-induct (i max)
    (declare (xargs :measure (nfix (- max i))))
    (if
        (and
            (natp i)
            (natp max)
            (< i max)
        )
        (cons i (inc-induct (+ 1 i) max))
        nil
    )
)

(defun inc-dec-induct (n1 n2 max1)
    (declare (xargs :measure (nfix (+ (- max1 n1) n2))))
    (if
        (and
            (natp n1)
            (natp n2)
            (natp max1)
            (< n1 max1)
            (< -1 n2)
        )
        (cons n1 (inc-dec-induct (+ 1 n1) (- n2 1) max1))
        nil
    )
)

(define sublist
    (
        (i integerp)
        (j integerp)
        (l true-listp)
    )
    :returns (result true-listp)
    (b*
        (
            (i (ifix i))
            (j (ifix j))
            (l (list-fix l))
            ((when (< i 0)) nil)
            ((when (< j 0)) nil)
            ((when (< j i)) nil)
            ((when (<= (length l) j)) nil)
        )
        (take (+ (- j i) 1) (nthcdr i l))
    )
    ///
    (fty::deffixequiv sublist))

(defrule sublist-1
    (implies
        (and
            (natp i)
            (natp j)
            (true-listp u)
            (true-listp v)
            (equal u v)
        )
        (equal
            (sublist i j u)
            (sublist i j v)
        )
    )
    :enable sublist
)

(defrule sublist-2
    (equal
        (nthcdr i u)
        (if
            (zp i)
            u
            (nthcdr (- i 1) (cdr u))
        )
    )
    :use ((:instance
          nthcdr-of-cons
          (n i)
          (a (car u))
          (x (cdr u))))
)

(defrule sublist-3
    (implies
        (and
            (natp i)
            (integer-listp u)
        )
        (integer-listp (nthcdr i u))
    )
    :disable (sublist sublist-1 sublist-2 nthcdr-of-cdr)
    :induct (cdr-dec-induct u i)
    :hints (("Subgoal *1/3" :use sublist-2))
)

(defrule sublist-5
    (implies
        (and
            (natp i)
            (integer-listp u)
            (< i (length u))
        )
        (integer-listp (take i u))
    )
    :disable (sublist sublist-1 sublist-2 sublist-3)
    :induct (cdr-dec-induct u i)
)

(defrule sublist-6
    (implies
        (and
            (natp i)
            (natp j)
            (integer-listp u)
        )
        (integer-listp (sublist i j u))
    )
    :enable sublist
    :disable (sublist-1 sublist-2 sublist-3 sublist-5)
    :use (sublist-3
          (:instance sublist-5
           (i (+ (- j i) 1)) (u (nthcdr i u))))
)

(defrule sublist-7
    (implies
        (and
            (natp i)
            (< i (len u))
        )
        (equal
            (sublist i i u)
            (take 1 (nthcdr i u))
        )
    )
    :hints (("Goal"
            :do-not-induct t
            :expand (sublist i i u)))
    :disable (car-of-nthcdr take-of-1 sublist-1 sublist-2
              sublist-3 sublist-5 sublist-6)
)

(defrule sublist-9
    (implies
        (and
            (natp i)
            (< i (len u))
        )
        (equal
            (sublist i i u)
            (cons (nth i u) nil)
        )
    )
    :disable (car-of-nthcdr take-of-1 sublist sublist-1 sublist-2
              sublist-3 sublist-5 sublist-6 sublist-7)
    :use (sublist-7
          (:instance car-of-nthcdr (i i) (x u))
          (:instance take-of-1 (x (nthcdr i u))))
    :do-not-induct t
)

(defrule sublist-10
    (implies
        (<= (len u) 0)
        (equal
            (sublist 0 0 u)
            nil
        )
    )
    :disable (car-of-nthcdr take-of-1 sublist-1 sublist-2
              sublist-3 sublist-5 sublist-6 sublist-7 sublist-9)
    :enable sublist
    :hints (("Goal" :cases ((true-listp u)))
            ("Subgoal 2" :expand (sublist 0 0 u)))
)

(defrule sublist-11
    (implies
        (and
            (natp i)
            (integerp v)
            (integer-listp u)
            (< i (len u))
        )
        (integer-listp
            (update-nth i v u)
        )
    )
    :disable (car-of-nthcdr take-of-1 sublist-1 sublist-2
              sublist-3 sublist-5 sublist-6 sublist-7 sublist-9 sublist-10)
    :induct (cdr-dec-induct u i)
)

(defrule sublist-12
    (implies
        (and
            (natp i)
            (integer-listp u)
            (< i (len u))
        )
        (integerp
            (nth i u)
        )
    )
    :disable (car-of-nthcdr take-of-1 sublist-1 sublist-2
              sublist-3 sublist-5 sublist-6 sublist-7 sublist-9 sublist-10 sublist-11)
    :induct (cdr-dec-induct u i)
)

(defrule sublist-13
    (implies
        (and
            (natp i)
            (natp j)
            (true-listp u)
            (<= i j)
            (< j (len u))
        )
        (equal
            (sublist i j u)
            (cons (nth i u) (sublist (+ i 1) j u))
        )
    )
    :do-not-induct t
    :disable (nthcdr take car-of-nthcdr nthcdr-of-cdr nthcdr-of-cons take-of-cons take-of-1 cons-car-cdr
              sublist-1 sublist-2 sublist-3 sublist-5 sublist-6 sublist-7 sublist-9 sublist-10 sublist-11)
    :enable (sublist)
    :hints (("Goal" :cases ((< j (+ i 1))))
            ("Subgoal 2"
             :use ((:instance nthcdr-of-cons (n (+ i 1)) (a (car u)) (x (cdr u)))
                   (:instance nthcdr-of-cdr (i i) (x u))
                   (:instance take-of-cons (n (+ 1 (- j i))) (a (car (nthcdr i u))) (x (cdr (nthcdr i u))))
                   (:instance car-of-nthcdr (i i) (x u))
                   (:instance cons-car-cdr (x u))
                   (:instance cons-car-cdr (x (nthcdr i u)))))
            ("Subgoal 1"
             :use ((:instance take-of-1 (x (nthcdr i u)))
                   (:instance car-of-nthcdr (i i) (x u)))))
)

(defrule sublist-14
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (< j (len u))
            (<= i k)
            (< k j)
        )
        (equal
            (sublist i j u)
            (append
                (sublist i k u)
                (sublist (+ k 1) j u)
            )
        )
    )
    :induct (inc-induct i (+ j 1))
    :disable (nthcdr take car-of-nthcdr nthcdr-of-cdr nthcdr-of-cons take-of-cons take-of-1 cons-car-cdr
              append-of-cons sublist-1 sublist-2 sublist-3 sublist-5 sublist-6 sublist-7 sublist-9
              sublist sublist-10 sublist-11 sublist-12 sublist-13)
    :enable (sublist)
    :hints (("Subgoal *1/1"
             :cases ((<= (+ i 1) k)))
            ("Subgoal *1/1.2"
             :do-not-induct t
             :use (sublist-9 sublist-13
                   (:instance append-of-cons
                    (a (nth i u))
                    (x nil)
                    (y (sublist (+ i 1) j u)))))
            ("Subgoal *1/1.1"
             :do-not-induct t
             :use (sublist-13
                   (:instance sublist-13 (j k))
                   (:instance append-of-cons
                    (a (nth i u))
                    (x (sublist (+ i 1) k u))
                    (y (sublist (+ k 1) j u))))))
)
