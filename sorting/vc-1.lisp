(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)
(include-book "tools/with-arith5-help" :dir :system)
(local (allow-arith5-help))

(include-book "permut")
(include-book "ordered")
(include-book "rep2")
(include-book "rep1")

(defrule vc-1-lemma-1
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (natp (+ i 1))
    )
)

(defrule vc-1-lemma-2
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (natp n)
    )
)

(defrule vc-1-lemma-3
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (<= (+ i 1) n)
    )
)

(defrule vc-1-lemma-4
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (integer-listp a0)
    )
)

(defrule vc-1-lemma-5
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (frame2->loop-break
                (frame2-init
                    (- i 1)
                    a
                )
            )
            nil
        )
    )
    :do-not-induct t
    :enable (frame2
             frame2->loop-break
             frame2-init)
)

(defrule vc-1-lemma-6
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (frame2->j
                (frame2-init
                    (- i 1)
                    a
                )
            )
            (- i 1)
        )
    )
    :do-not-induct t
    :enable (frame2
             frame2->j
             frame2-init)
    :disable (sublist-1)
)

(defrule vc-1-lemma-7
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (frame2->a
                (frame2-init
                    (- i 1)
                    a
                )
            )
            a
        )
    )
    :do-not-induct t
    :enable (frame2
             frame2->a
             frame2-init)
    :disable (sublist-1)
)

(defrule vc-1-lemma-8
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (envir2->upper-bound
                (envir2-init
                    (+ (- i 1) 1)
                    (nth i a)
                )
            )
            (+ (- i 1) 1)
        )
    )
    :do-not-induct t
    :enable (envir2
             envir2->upper-bound
             envir2-init)
    :disable (sublist-1)
)

(defrule vc-1-lemma-9
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (envir2->k
                (envir2-init
                    (+ (- i 1) 1)
                    (nth i a)
                )
            )
            (nth i a)
        )
    )
    :do-not-induct t
    :enable (envir2
             envir2->k
             envir2-init)
    :disable (sublist-1)
)

(defrule vc-1-lemma-10
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (and
            (natp (+ (- i 1) 1))
            (<=
                (+ (- i 1) 1)
                (envir2->upper-bound
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                )
            )
            (<
                (envir2->upper-bound
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                )
                (len
                    (frame2->a
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (equal
                (frame2->j
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
                (-
                    (envir2->upper-bound
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                    )
                    1
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              sublist-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9)
)

(defrule vc-1-lemma-11
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (integerp (nth i a))
    )
    :disable (sublist-1)
    :use ((:instance sublist-12
           (i i)
           (u a)))
)

(defrule vc-1-lemma-12
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (integer-listp
            (frame2->a
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              sublist-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-5
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-13
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (natp
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              sublist-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-46
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-14
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (<
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (len
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              sublist-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-42
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-15
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (integer-listp
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-12
              vc-1-lemma-13
              vc-1-lemma-14
              sublist-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          vc-1-lemma-11
          vc-1-lemma-12
          vc-1-lemma-13
          vc-1-lemma-14
          (:instance sublist-11
            (i
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
            )
            (v (nth i a))
            (u
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )))
)

(defrule vc-1-lemma-16
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (=
            (len a)
            (len
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              sublist-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-7
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-17
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (<=
            n
            (len
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-12
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-16
              sublist-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          vc-1-lemma-11
          vc-1-lemma-12
          vc-1-lemma-13
          vc-1-lemma-14
          vc-1-lemma-16)
)

(defrule vc-1-lemma-18
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                a
            )
            (sublist
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-76
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-19
    (implies
        (and
            (<
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                0
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
            (frame2->a
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1
              sublist-2)
)

(defrule vc-1-lemma-20
    (implies
        (and
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
            (frame2->a
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-12
              vc-1-lemma-16
              vc-1-lemma-18
              sublist-1
              sublist-2)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          vc-1-lemma-12
          vc-1-lemma-18
          (:instance permutation-1
            (i 0)
            (j
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (u a)
            (v
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )))
)

(defrule vc-1-lemma-21
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
            (frame2->a
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1
              sublist-2)
    :hints (("Goal" :cases
           (
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
           ))
            ("Subgoal 2" :use (vc-1-lemma-19))
            ("Subgoal 1" :use (vc-1-lemma-20))
)
)

(defrule vc-1-lemma-22
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                a
            )
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1
              sublist-2)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-56
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-23
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            a
            (frame2->a
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-12
              vc-1-lemma-13
              vc-1-lemma-16
              vc-1-lemma-22
              sublist-1
              sublist-2)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          vc-1-lemma-12
          vc-1-lemma-13
          vc-1-lemma-22
          (:instance permutation-1
            (i 0)
            (j
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (u a)
            (v
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )))
)

(defrule vc-1-lemma-24
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (natp
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                2
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1
              sublist-2)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-40
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-25
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (<
            0
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                2
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1
              sublist-2)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-44
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-26
    (implies
        (and
            (natp index)
            (>=
                index
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
            )
            (<= index i)
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (nth
                (- index 1)
                a
            )
            (nth
                index
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1
              sublist-2)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-39
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-27
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (- i 1)
                a
            )
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1
              sublist-2)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-55
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-28
    (implies
        (and
            (<
                i
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (- i 1)
                a
            )
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist-1
              sublist-2)
)

(defrule vc-1-lemma-29
    (implies
        (and
            (natp bound)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                bound
            )
            (<= bound i)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal-ranges
            (- bound 1)
            (- i 1)
            bound
            i
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :induct (inc-induct bound (+ i 1))
    :enable (equal-ranges)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              vc-1-lemma-25
              vc-1-lemma-26
              sublist-1
              sublist-2
              permutation-3
              permutation-5
              permutation-6
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Subgoal *1/1"
                 :cases ((<= (+ bound 1) i)))
                ("Subgoal *1/1.2"
                 :do-not-induct t
                 :use (vc-1-lemma-16 vc-1-lemma-25
                      (:instance vc-1-lemma-26 (index bound)))
                 :expand
                     (equal-ranges
                         (- bound 1)
                         (- i 1)
                         bound
                         i
                         a
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
                     )
                )
                ("Subgoal *1/1.1"
                 :do-not-induct t
                 :use (vc-1-lemma-16 vc-1-lemma-25
                      (:instance vc-1-lemma-26 (index bound)))
                 :expand
                     (equal-ranges
                         (- bound 1)
                         (- i 1)
                         bound
                         i
                         a
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
                     )
                ))
        )
)

(defrule vc-1-lemma-30
    (implies
        (and
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal-ranges
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                2
            )
            i
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-29
              sublist-1
              sublist-2
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-24
          (:instance vc-1-lemma-29
           (bound
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
           )))
)

(defrule vc-1-lemma-31
    (implies
        (and
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (- i 1)
                a
            )
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-29
              vc-1-lemma-30
              sublist
              sublist-1
              sublist-2
              sublist-14
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-15
          vc-1-lemma-17
          vc-1-lemma-24
          vc-1-lemma-30
          (:instance equal-ranges-sublists
           (i1
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
           )
           (j1 (- i 1))
           (i2
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
           )
           (j2 i)
           (l1 a)
           (l2
                    (update-nth
                        (+
                            (frame2->j
                                (rep2
                                    (+ (- i 1) 1)
                                    (envir2-init
                                        (+ (- i 1) 1)
                                        (nth i a)
                                    )
                                    (frame2-init
                                        (- i 1)
                                        a
                                    )
                                )
                            )
                            1
                        )
                        (nth i a)
                        (frame2->a
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                    )
           )
           ))
)

(defrule vc-1-lemma-32
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (- i 1)
                a
            )
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              sublist
              sublist-1
              sublist-2
              sublist-14
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases
           (
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
           ))
            ("Subgoal 2" :use (vc-1-lemma-28))
            ("Subgoal 1" :use (vc-1-lemma-31))
)
)

(defrule vc-1-lemma-33
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (nth i a)
            (nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              sublist
              sublist-1
              sublist-2
              sublist-14)
)

(defrule vc-1-lemma-34
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (list (nth i a))
            (list
                (nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (update-nth
                        (+
                            (frame2->j
                                (rep2
                                    (+ (- i 1) 1)
                                    (envir2-init
                                        (+ (- i 1) 1)
                                        (nth i a)
                                    )
                                    (frame2-init
                                        (- i 1)
                                        a
                                    )
                                )
                            )
                            1
                        )
                        (nth i a)
                        (frame2->a
                           (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-16
              vc-1-lemma-33
              sublist
              sublist-1
              sublist-2
              sublist-14)
    :use (vc-1-lemma-33)
)

(defrule vc-1-lemma-35
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (list (nth i a))
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-33
              vc-1-lemma-34
              sublist
              sublist-1
              sublist-2
              sublist-14
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-14
          vc-1-lemma-34
          (:instance sublist-9
           (i
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
           )
           (u
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
           )
         ))
)

(defrule vc-1-lemma-36
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (list (nth i a))
            (sublist
                i
                i
                a
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              sublist-1
              sublist-2
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use ((:instance sublist-9
           (i i)
           (u a)
         ))
)

(defrule vc-1-lemma-37
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                i
                i
                a
            )
            (sublist
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              sublist-1
              sublist-2
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-35 vc-1-lemma-36)
)

(defrule vc-1-lemma-38
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (<=
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            i
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              sublist-1
              sublist-2
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-6
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-39
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              sublist
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              rep2)
    :use (vc-1-lemma-13
          vc-1-lemma-15
          vc-1-lemma-17
          vc-1-lemma-24
          vc-1-lemma-32
          vc-1-lemma-37
          vc-1-lemma-38
          (:instance permutation-9
           (i
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
           )
           (k
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
           )
           (j i)
           (u
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
           )
           (v a)
           ))
)

(defrule vc-1-lemma-40
    (implies
        (and
            (<
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                0
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                a
            )
            (sublist
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              rep2)
)

(defrule vc-1-lemma-41
    (implies
        (and
            (natp index)
            (<=
                index
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (nth
                index
                a
            )
            (nth
                index
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              sublist
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              rep2)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-27
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-42
    (implies
        (and
            (natp index)
            (<=
                index
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (nth
                index
                a
            )
            (nth
                index
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              sublist
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              rep2)
    :use (vc-1-lemma-41)
)

(defrule vc-1-lemma-43
    (implies
        (and
            (natp index)
            (<=
                index
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (nth
                index
                a
            )
            (nth
                index
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              sublist
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              rep2
              update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same)
    :use (vc-1-lemma-13
          vc-1-lemma-14
          vc-1-lemma-16
          vc-1-lemma-42
          (:instance
           nth-of-update-nth-diff
           (n1 index)
           (n2
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
           )
           (x
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
           )
          ))
)

(defrule vc-1-lemma-44
    (implies
        (and
            (natp bound)
            (<=
                bound
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal-ranges
            bound
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            bound
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :induct (inc-induct bound
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
            )
    :enable (equal-ranges)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              sublist
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Subgoal *1/1"
                 :cases ((<=
                         (+ bound 1)
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    ))))
                ("Subgoal *1/1.2"
                 :do-not-induct t
                 :use (vc-1-lemma-13 vc-1-lemma-14
                      vc-1-lemma-16 vc-1-lemma-25
                      (:instance vc-1-lemma-43 (index bound)))
                 :expand
                     (equal-ranges
                         bound
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
                         bound
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
                         a
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
                     )
                )
                ("Subgoal *1/1.1"
                 :do-not-induct t
                 :use (vc-1-lemma-13 vc-1-lemma-14
                      vc-1-lemma-16 vc-1-lemma-25
                      (:instance vc-1-lemma-43 (index bound)))
                 :expand
                     (equal-ranges
                         bound
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
                         bound
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
                         a
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
                     )
                ))
        )
)

(defrule vc-1-lemma-45
    (implies
        (and
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal-ranges
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              sublist
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (:instance vc-1-lemma-44
          (bound 0))
)

(defrule vc-1-lemma-46
    (implies
        (and
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                a
            )
            (sublist
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              sublist
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-14
          vc-1-lemma-15
          vc-1-lemma-16
          vc-1-lemma-17
          vc-1-lemma-24
          vc-1-lemma-45
          (:instance equal-ranges-sublists
           (i1 0)
           (j1
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
           )
           (i2 0)
           (j2
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
           )
           (l1 a)
           (l2
                    (update-nth
                        (+
                            (frame2->j
                                (rep2
                                    (+ (- i 1) 1)
                                    (envir2-init
                                        (+ (- i 1) 1)
                                        (nth i a)
                                    )
                                    (frame2-init
                                        (- i 1)
                                        a
                                    )
                                )
                            )
                            1
                        )
                        (nth i a)
                        (frame2->a
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                    )
           )
           ))
)

(defrule vc-1-lemma-47
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                a
            )
            (sublist
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              sublist
              sublist-1
              sublist-2
              sublist-13
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases
           (
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
           ))
            ("Subgoal 2" :use (vc-1-lemma-40))
            ("Subgoal 1" :use (vc-1-lemma-46))
)
)

(defrule vc-1-lemma-48
    (implies
        (and
            (<
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                0
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47              
              sublist-1
              sublist-2
              sublist-13              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
)

(defrule vc-1-lemma-49
    (implies
        (and
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              sublist            
              sublist-1
              sublist-2
              sublist-13
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          vc-1-lemma-15
          vc-1-lemma-47
          (:instance permutation-1
            (i 0)
            (j
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (u a)
            (v
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )))
)

(defrule vc-1-lemma-50
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              sublist            
              sublist-1
              sublist-2
              sublist-13
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases
           (
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
           ))
            ("Subgoal 2" :use (vc-1-lemma-48))
            ("Subgoal 1" :use (vc-1-lemma-49)))
)

(defrule vc-1-lemma-51
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            i
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              sublist            
              sublist-1
              sublist-2
              sublist-13
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-15
          vc-1-lemma-39
          (:instance permutation-5
           (i
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
           )
           (j i)
           (u
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
           )
           (v a)
           ))
)

(defrule vc-1-lemma-52
    (implies
        (and
            (<
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                0
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            i
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              sublist            
              sublist-1
              sublist-2
              sublist-13
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-51)
)

(defrule vc-1-lemma-53
    (implies
        (and
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            i
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              sublist            
              sublist-1
              sublist-2
              sublist-13
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-15
          vc-1-lemma-17
          vc-1-lemma-38
          vc-1-lemma-50
          vc-1-lemma-51
          (:instance permutation-7
           (i 0)
           (k
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
           )
           (j i)
           (u a)
           (v
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
           )))
)

(defrule vc-1-lemma-54
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            i
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              sublist            
              sublist-1
              sublist-2
              sublist-13
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases
           (
            (<=
                0
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
           ))
            ("Subgoal 2" :use (vc-1-lemma-52))
            ("Subgoal 1" :use (vc-1-lemma-53)))
)

(defrule vc-1-lemma-55
    (implies
        (and
            (natp index)
            (< i index)
            (< index (len a))
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (nth
                index
                a
            )
            (nth
                index
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              sublist            
              sublist-1
              sublist-2
              sublist-13
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-78
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-56
    (implies
        (and
            (natp index)
            (< i index)
            (< index (len a))
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (nth
                index
                a
            )
            (nth
                index
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              sublist            
              sublist-1
              sublist-2
              sublist-13
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              rep2
              update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same)
    :use (vc-1-lemma-13
          vc-1-lemma-14
          vc-1-lemma-16
          vc-1-lemma-38
          vc-1-lemma-55
          (:instance
           nth-of-update-nth-diff
           (n1 index)
           (n2
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
           )
           (x
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
           )
          ))
)

(defrule vc-1-lemma-57
    (implies
        (and
            (natp bound)
            (<= (+ i 1) bound)
            (<= bound (- n 1))
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal-ranges
            bound
            (- n 1)
            bound
            (- n 1)
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :induct (inc-induct bound n)
    :enable (equal-ranges)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              sublist            
              sublist-1
              sublist-2
              sublist-9
              sublist-13
              sublist-14
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges-sublists
              equal-ranges-sublists-1
              rep2
              update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same)
    :hints (("Subgoal *1/1"
                 :cases ((<=
                         (+ bound 1)
                         (- n 1))))
                ("Subgoal *1/1.2"
                 :do-not-induct t
                 :use (vc-1-lemma-13 vc-1-lemma-14
                      vc-1-lemma-16 vc-1-lemma-25
                      (:instance vc-1-lemma-56 (index bound)))
                 :expand
                     (equal-ranges
                         bound
                         (- n 1)
                         bound
                         (- n 1)
                         a
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
                     )
                )
                ("Subgoal *1/1.1"
                 :do-not-induct t
                 :use (vc-1-lemma-13 vc-1-lemma-14
                      vc-1-lemma-16 vc-1-lemma-25
                      (:instance vc-1-lemma-56 (index bound)))
                 :expand
                     (equal-ranges
                         bound
                         (- n 1)
                         bound
                         (- n 1)
                         a
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
                     )
                ))
        )
)

(defrule vc-1-lemma-58
    (implies
        (and
            (< (+ i 1) n)
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal-ranges
            (+ i 1)
            (- n 1)
            (+ i 1)
            (- n 1)
            a
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              sublist            
              sublist-1
              sublist-2
              sublist-9
              sublist-13
              sublist-14
              permutation              
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use ((:instance vc-1-lemma-57
           (bound (+ i 1))))
)

(defrule vc-1-lemma-59
    (implies
        (and
            (<= n (+ i 1))
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                (+ i 1)
                (- n 1)
                a
            )
            (sublist
                (+ i 1)
                (- n 1)
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (permutation sublist)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
)

(defrule vc-1-lemma-60
    (implies
        (and
            (< (+ i 1) n)
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                (+ i 1)
                (- n 1)
                a
            )
            (sublist
                (+ i 1)
                (- n 1)
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              rep2)
    :use (vc-1-lemma-13
          vc-1-lemma-14
          vc-1-lemma-15
          vc-1-lemma-16
          vc-1-lemma-17
          vc-1-lemma-24
          vc-1-lemma-58
          (:instance equal-ranges-sublists
           (i1 (+ i 1))
           (j1 (- n 1))
           (i2 (+ i 1))
           (j2 (- n 1))
           (l1 a)
           (l2
                    (update-nth
                        (+
                            (frame2->j
                                (rep2
                                    (+ (- i 1) 1)
                                    (envir2-init
                                        (+ (- i 1) 1)
                                        (nth i a)
                                    )
                                    (frame2-init
                                        (- i 1)
                                        a
                                    )
                                )
                            )
                            1
                        )
                        (nth i a)
                        (frame2->a
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                    )
           )
           ))
)

(defrule vc-1-lemma-61
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist
                (+ i 1)
                (- n 1)
                a
            )
            (sublist
                (+ i 1)
                (- n 1)
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases ((< (+ i 1) n)))
            ("Subgoal 2" :use (vc-1-lemma-59))
            ("Subgoal 1" :use (vc-1-lemma-60)))
)

(defrule vc-1-lemma-62
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (len a)
            (len
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-12
          vc-1-lemma-13
          vc-1-lemma-14
          vc-1-lemma-15
          vc-1-lemma-16)
)

(defrule vc-1-lemma-63
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (len a0)
            (len
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-62)
)

(defrule vc-1-lemma-64
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist i (- n 1) a0)
            (cons
                (nth i a0)
                (sublist (+ i 1) (- n 1) a0)
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use ((:instance sublist-13
           (i i)
           (j (- n 1))
           (u a0)))
)

(defrule vc-1-lemma-65
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist i (- n 1) a)
            (cons
                (nth i a)
                (sublist (+ i 1) (- n 1) a)
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use ((:instance sublist-13
           (i i)
           (j (- n 1))
           (u a)))
)

(defrule vc-1-lemma-66
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist (+ i 1) (- n 1) a0)
            (sublist (+ i 1) (- n 1) a)
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-64
          vc-1-lemma-65)
)

(defrule vc-1-lemma-67
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist (+ i 1) (- n 1) a0)   
            (sublist
                (+ i 1)
                (- n 1)
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-61
          vc-1-lemma-66)
)

(defrule vc-1-lemma-68
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (nth i a0)   
            (nth i a)
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-64
          vc-1-lemma-65)
)

(defrule vc-1-lemma-69
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (list (nth i a0))   
            (list (nth i a))
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-68)
)

(defrule vc-1-lemma-70
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (list (nth i a0))   
            (sublist i i a0)
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use ((:instance sublist-9
           (i i)
           (u a0)))
)

(defrule vc-1-lemma-71
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (list (nth i a))   
            (sublist i i a)
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use ((:instance sublist-9
           (i i)
           (u a)))
)

(defrule vc-1-lemma-72
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (equal
            (sublist i i a0)   
            (sublist i i a)
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-69
          vc-1-lemma-70
          vc-1-lemma-71)
)

(defrule vc-1-lemma-73
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            i
            i
            a0
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-72
          (:instance permutation-1
           (i i)
           (j i)
           (u a0)
           (v a)))
)

(defrule vc-1-lemma-74
    (implies
        (and
            (< (- i 1) 0)
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            i
            a0
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-73)
)

(defrule vc-1-lemma-75
    (implies
        (and
            (<= 0 (- i 1))
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            i
            a0
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-73
          (:instance permutation-7
           (i 0)
           (j i)
           (k (- i 1))
           (u a0)
           (v a)))
)

(defrule vc-1-lemma-76
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            i
            a0
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases ((<= 0 (- i 1))))
            ("Subgoal 2" :use (vc-1-lemma-74))
            ("Subgoal 1" :use (vc-1-lemma-75)))
)

(defrule vc-1-lemma-77
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (permutation
            0
            i
            a0
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-15
          vc-1-lemma-54
          vc-1-lemma-76
          (:instance permutation-6
           (i 0)
           (j i)
           (u a0)
           (v a)
           (w
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
           )))
)

(defrule vc-1-lemma-78
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
        )
        (<
            (nth i a)
            (nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-83
           (iteration (+ (- i 1) 1))
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-79
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
        (<=
            (nth
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (nth i a)
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-5
          vc-1-lemma-6
          vc-1-lemma-7
          vc-1-lemma-8
          vc-1-lemma-9
          vc-1-lemma-10
          (:instance rep2-lemma-108
           (env2
               (envir2-init
                   (+ (- i 1) 1)
                   (nth i a)
               )
           )
           (fr2
               (frame2-init
                   (- i 1)
                   a
               )
           )))
)

(defrule vc-1-lemma-80
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
        )
        (<
            (nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
            (nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-33
          vc-1-lemma-78)
)

(defrule vc-1-lemma-81
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
        (<=
            (nth
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-33
          vc-1-lemma-79)
)

(defrule vc-1-lemma-82
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
        )
        (<
            (nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
            (nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-80
          (:instance
           nth-of-update-nth-diff
            (n1
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
            )
            (n2
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
            )
            (v (nth i a))
            (x
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )))
)

(defrule vc-1-lemma-83
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
        (<=
            (nth
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
            (nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (update-nth
                    (+
                        (frame2->j
                            (rep2
                                (+ (- i 1) 1)
                                (envir2-init
                                    (+ (- i 1) 1)
                                    (nth i a)
                                )
                                (frame2-init
                                    (- i 1)
                                    a
                                )
                            )
                        )
                        1
                    )
                    (nth i a)
                    (frame2->a
                        (rep2
                           (+ (- i 1) 1)
                           (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-81
          (:instance
           nth-of-update-nth-diff
            (n1
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (n2
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
            )
            (v (nth i a))
            (x
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )))
)

(defrule vc-1-lemma-84
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                0
            )
        )
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
        )
    )
    :do-not-induct t
    :enable (ordered orderedp sublist)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :expand
            (
                (ordered
                    0
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    a
                )
                (sublist
                    0
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    a
                )
            )))
)

(defrule vc-1-lemma-85
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (=
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (- i 1)
            )
        )
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
)

(defrule vc-1-lemma-86
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (<
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (- i 1)
            )
        )
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-38
          (:instance ordered-4
           (i 0)
           (j (- i 1))
           (k
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
           )
           (u a)
          ))
)

(defrule vc-1-lemma-87
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            (<=
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                (- i 1)
            )
        )
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-85
          vc-1-lemma-86)
)

(defrule vc-1-lemma-88
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-38
          vc-1-lemma-87)
)

(defrule vc-1-lemma-89
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases
           (
                (natp
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                )
            )
           )
           ("Subgoal 2" :use (vc-1-lemma-84))
           ("Subgoal 1" :use (vc-1-lemma-88)))
)

(defrule vc-1-lemma-90
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (ordered)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :use
            (
                vc-1-lemma-15
                vc-1-lemma-47
                vc-1-lemma-89
            )
            :expand
            (
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            a
        )
        (ordered
            0
            (frame2->j
                (rep2
                    (+ (- i 1) 1)
                    (envir2-init
                        (+ (- i 1) 1)
                        (nth i a)
                    )
                    (frame2-init
                        (- i 1)
                        a
                    )
                )
            )
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
            )
            ))
)

(defrule vc-1-lemma-91
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<
                (- i 1)
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
            )
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            a
        )
    )
    :do-not-induct t
    :enable (ordered orderedp sublist)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal"
            :expand
            (
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            a
        )
        (sublist
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            a
        )
            )
            ))
)

(defrule vc-1-lemma-92
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (- i 1)
            )
            (<
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                0
            )
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13)
)

(defrule vc-1-lemma-93
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (- i 1)
            )
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          (:instance ordered-5
           (i 0)
           (j (- i 1))
           (k
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
           )
           (u a)
          ))
)

(defrule vc-1-lemma-94
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (- i 1)
            )
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases
           (
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            )
           )
           ("Subgoal 2" :use (vc-1-lemma-92))
           ("Subgoal 1" :use (vc-1-lemma-93)))
)

(defrule vc-1-lemma-95
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            a
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :cases
           (
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (- i 1)
            )
            )
           )
           ("Subgoal 2" :use (vc-1-lemma-91))
           ("Subgoal 1" :use (vc-1-lemma-94)))
)

(defrule vc-1-lemma-96
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                2
            )
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :enable (ordered)
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :hints (("Goal" :use
            (
                vc-1-lemma-15
                vc-1-lemma-32
                vc-1-lemma-95
            )
            :expand
            (
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            (- i 1)
            a
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                2
            )
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
            )
            ))
)

(defrule vc-1-lemma-97
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<
                i
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
            )
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1)
    :use (vc-1-lemma-13
          vc-1-lemma-15
          vc-1-lemma-38
          (:instance ordered-1
           (i
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
           )
           (u
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            ))))
)

(defrule vc-1-lemma-98
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :use (vc-1-lemma-13
          vc-1-lemma-15
          vc-1-lemma-38
          vc-1-lemma-62
          vc-1-lemma-82
          vc-1-lemma-96
          (:instance ordered-1
           (i
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
           )
           (u
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )))
          (:instance ordered-3
           (i
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
           )
           (j i)
           (k
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
           )
           (u
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            ))))
)

(defrule vc-1-lemma-99
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (ordered
            (+
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                1
            )
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :hints (("Goal" :cases
           (
            (<=
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    2
                )
                i
            )
            )
           )
           ("Subgoal 2" :use (vc-1-lemma-97))
           ("Subgoal 1" :use (vc-1-lemma-98)))
)

(defrule vc-1-lemma-100
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (<
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
                0
            )
        )
        (ordered
            0
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :use (vc-1-lemma-13
          vc-1-lemma-99)
)

(defrule vc-1-lemma-101
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
        (ordered
            0
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :use (vc-1-lemma-13
          vc-1-lemma-15
          vc-1-lemma-38
          vc-1-lemma-62
          vc-1-lemma-83
          vc-1-lemma-90
          vc-1-lemma-99
          (:instance ordered-3
           (i 0)
           (j i)
           (k
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
           )
           (u
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            ))))
)

(defrule vc-1-lemma-102
    (implies
        (and
            (< i n)
            (natp i)
            (natp n)
            (<= i n)
            (integer-listp a0)
            (integer-listp a)
            (<= n (len a))
            (equal
                (len a0)
                (len a)
            )
            (equal
                (sublist i (- n 1) a0)
                (sublist i (- n 1) a)
            )
            (permutation 0 (- i 1) a0 a)
            (ordered 0 (- i 1) a)
        )
        (ordered
            0
            i
            (update-nth
                (+
                    (frame2->j
                        (rep2
                            (+ (- i 1) 1)
                            (envir2-init
                                (+ (- i 1) 1)
                                (nth i a)
                            )
                            (frame2-init
                                (- i 1)
                                a
                            )
                        )
                    )
                    1
                )
                (nth i a)
                (frame2->a
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :hints (("Goal" :cases
           (
            (natp
                (frame2->j
                    (rep2
                        (+ (- i 1) 1)
                        (envir2-init
                            (+ (- i 1) 1)
                            (nth i a)
                        )
                        (frame2-init
                            (- i 1)
                            a
                        )
                    )
                )
            )
            )
           )
           ("Subgoal 2" :use (vc-1-lemma-100))
           ("Subgoal 1" :use (vc-1-lemma-101)))
)

(defrule vc-1-lemma-103
    (implies
        (and
            (<= n 1)
            (natp n)
            (< 0 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (and
            (permutation
                0
                (- n 1)
                a0
                (frame1->a
                    (rep1
                        (-
                            n
                            1
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
            (ordered
                0
                (- n 1)
                (frame1->a
                    (rep1
                        (-
                            n
                            1
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
        )
    )
    :do-not-induct t
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-3)
    :enable
    (
        frame1-init
        rep1
        ordered-1
        frame1
        frame1->a
        frame1-fix
        integer-list-fix
    )
    :hints
    (
        ("Goal"
            :expand
            (
                (rep1
                    (-
                        n
                        1
                    )
                    (envir1-init
                        1
                    )
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                )
            )
        )
    )
)

(defrule vc-1-lemma-105
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (integer-listp
            (frame1->a
                (rep1
                    (-
                        n
                        2
                    )
                    (envir1-init
                        1
                    )
                    (frame1-init
                       1
                       0
                       0
                       a
                    )
                )
            )
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              rep1-lemma-5
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)

    :hints
    (
        ("Goal"
         :use
         (   
            (:instance
                rep1-lemma-5
                (iteration
                    (-
                        n
                        2
                    )
                )
                (env1
                    (envir1-init
                        1
                    )
                )
                (fr1
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                )
            )
         )
         :do-not-induct t
        )
    )
)

(defrule vc-1-lemma-106
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (envir1->lower-bound
                (envir1-init
                    1
                )
            )
            1
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              rep1-lemma-5
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :enable (envir1
              envir1-fix
              envir1-init
              envir1->lower-bound)
    :do-not-induct t
)

(defrule vc-1-lemma-107
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (frame1->a
                (frame1-init
                    1
                    0
                    0
                    a
                )
            )
            a
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              rep1-lemma-5
              rep1-lemma-9
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :enable (frame1
              frame1-fix
              frame1-init
              frame1->a
    )
    :do-not-induct t
)

(defrule vc-1-lemma-108
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (len
                (frame1->a
                    (rep1
                        (-
                           n
                           2
                        )
                        (envir1-init
                           1
                        )
                        (frame1-init
                           1
                           0
                           0
                           a
                        )
                    )
                )
            )
            (len a)
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)

    :hints
    (
        ("Goal"
         :use
         (
            vc-1-lemma-106  
            vc-1-lemma-107 
            (:instance
                rep1-lemma-9
                (iteration
                    (-
                        n
                        2
                    )
                )
                (env1
                    (envir1-init
                        1
                    )
                )
                (fr1
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                )
            )
         )
         :do-not-induct t
        )
    )
)

(defrule vc-1-lemma-109
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (frame2->a
                (frame2-init
                    (-
                        n
                        2
                    )
                    (frame1->a
                        (rep1
                           (-
                               n
                               2
                           )
                           (envir1-init
                               1
                           )
                           (frame1-init
                               1
                               0
                               0
                               a
                           )
                        )
                    )
                )
            )
            (frame1->a
                (rep1
                    (-
                        n
                        2
                    )
                    (envir1-init
                        1
                    )
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                )
            )
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :enable (frame2
              frame2-fix
              frame2-init
              frame2->a)
    :do-not-induct t
)

(defrule vc-1-lemma-110
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (frame2->j
                (frame2-init
                    (-
                        n
                        2
                    )
                    (frame1->a
                        (rep1
                           (-
                               n
                               2
                           )
                           (envir1-init
                               1
                           )
                           (frame1-init
                               1
                               0
                               0
                               a
                           )
                        )
                    )
                )
            )
            (-
                n
                2
            )
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :enable (frame2
              frame2-fix
              frame2-init
              frame2->j)
    :do-not-induct t
)

(defrule vc-1-lemma-111
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (envir2->upper-bound
                (envir2-init
                    (-
                        n
                        1
                    )
                    (nth
                        (-
                            n
                            1
                        )
                        (frame1->a
                           (rep1
                               (-
                                   n
                                   2
                               )
                               (envir1-init
                                   1
                               )
                               (frame1-init
                                   1
                                   0
                                   0
                                   a
                               )
                           )
                        )
                    )
                )
            )
            (-
                n
                1
            )
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              vc-1-lemma-110
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :enable (envir2
              envir2-fix
              envir2-init
              envir2->upper-bound)
    :do-not-induct t
)

(defrule vc-1-lemma-112
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (frame1->loop-break
                (frame1-init
                    1
                    0
                    0
                    a
                )
            )
            nil
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              vc-1-lemma-110
              vc-1-lemma-111
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep1-lemma-14
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :enable (frame1
              frame1-fix
              frame1-init
              frame1->loop-break
    )
    :do-not-induct t
)

(defrule vc-1-lemma-113
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (sublist
                (- n 1)
                (- n 1)
                (frame1->a
                    (rep1
                        (-
                           n
                           2
                        )
                        (envir1-init
                           1
                        )
                        (frame1-init
                           1
                           0
                           0
                           a
                        )
                    )
                )
            )
            (sublist
                (- n 1)
                (- n 1)
                a
            )
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              vc-1-lemma-110
              vc-1-lemma-111
              vc-1-lemma-112
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep1-lemma-14
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :hints
    (
        ("Goal"
         :use
         (
            vc-1-lemma-106  
            vc-1-lemma-107
            vc-1-lemma-112
            (:instance
                rep1-lemma-14
                (iteration
                    (-
                        n
                        2
                    )
                )
                (env1
                    (envir1-init
                        1
                    )                    
                )
                (fr1
                    (frame1-init
                        1
                        0
                        0
                        a
                    ) 
                )
            )
         )
         :do-not-induct t
        )
    )
)

(defrule vc-1-lemma-114
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (sublist
                (- n 1)
                (- n 1)
                (frame1->a
                    (rep1
                        (-
                           n
                           2
                        )
                        (envir1-init
                           1
                        )
                        (frame1-init
                           1
                           0
                           0
                           a
                        )
                    )
                )
            )
            (sublist
                (- n 1)
                (- n 1)
                a0
            )
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              vc-1-lemma-110
              vc-1-lemma-111
              vc-1-lemma-112
              vc-1-lemma-113
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep1-lemma-14
              rep2
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :hints
    (
        ("Goal"
         :use
         (
            vc-1-lemma-113
            (:instance
                sublist-1
                (i
                    (-
                        n
                        1
                    )
                )
                (j
                    (-
                        n
                        1
                    )
                )
                (u a0)
                (v a)
            )
         )
         :do-not-induct t
        )
    )
)

(defrule vc-1-lemma-115
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (equal
            (frame1->a
                (rep1
                    (-
                        n
                        1
                    )
                    (envir1-init
                        1
                    )
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                 )
             )
             (update-nth
                             (+
                             (frame2->j
                                 (rep2
                             (-
                                 n
                                 1
                             )
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                                 )
                             )
                             1
                             )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )  
                        (frame2->a
                                 (rep2
                             (-
                                 n
                                 1
                             )
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                                 )
                             )
             )
         )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              vc-1-lemma-110
              vc-1-lemma-111
              vc-1-lemma-112
              vc-1-lemma-113
              vc-1-lemma-114
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep1-lemma-10
              rep1-lemma-14
              rep2
              rep2-lemma-5
              rep2-lemma-6
              rep2-lemma-7
              rep2-lemma-12
              rep2-lemma-46
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-11
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :hints
    (
        ("Goal"
        :expand
        (
                (rep1
                    (-
                        n
                        1
                    )
                    (envir1-init
                        1
                    )
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                 )
        )
         :use
         (
            vc-1-lemma-105
            vc-1-lemma-108
            vc-1-lemma-114
            vc-1-lemma-106  
            vc-1-lemma-107
            vc-1-lemma-112
            vc-1-lemma-109  
            vc-1-lemma-110
            vc-1-lemma-111
                    (:instance
                         rep2-lemma-6
                         (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                     (:instance
                         rep2-lemma-7
                         (iteration
                            (-
                               n
                               1
                            )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                    (:instance
                         rep2-lemma-5
                          (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                    (:instance
                         sublist-12
                         (i
                                      (-
                                          n
                                          1
                                      )
                         )
                         (u
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                         )
                     )
                     (:instance
                         sublist-11
                         (i
                             (+
                             (frame2->j
                                 (rep2
                             (-
                                 n
                                 1
                             )
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                                 )
                             )
                             1
                             )
                         )
                         (v
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )  
                         )
                         (u
                            (frame2->a
                                 (rep2
                             (-
                                 n
                                 1
                             )
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                                 )
                             )
                         )
                     )
                     (:instance
                         rep2-lemma-46
                         (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     

                      )
                      (:instance
                          rep1-lemma-10
                          (iteration (- n 2))
                          (env1
                                           (envir1-init
                                               1
                                           )
                          )
                          (fr1
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                          )
                      )
                     (:instance
                          rep1-lemma-10
                          (iteration (- n 1))
                          (env1
                                           (envir1-init
                                               1
                                           )
                          )
                          (fr1
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                          )
                      )
              
         )
         :do-not-induct t
        )
    )
)

(defrule vc-1-lemma-116
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (permutation
                0
                (- n 2)
                a0
                (frame1->a
                    (rep1
                        (-
                            n
                            2
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
            (ordered
                0
                (- n 2)
                (frame1->a
                    (rep1
                        (-
                            n
                            2
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
        )
        (permutation
            0
            (- n 1)
            a0
            (frame1->a
                (rep1
                    (-
                        n
                        1
                    )
                    (envir1-init
                        1
                    )
                    (frame1-init
                        1
                        0
                        0
                        a
                    )
                 )
             )
         )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              vc-1-lemma-110
              vc-1-lemma-111
              vc-1-lemma-112
              vc-1-lemma-113
              vc-1-lemma-114
              vc-1-lemma-115
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep1-lemma-10
              rep1-lemma-14
              rep2
              rep2-lemma-5
              rep2-lemma-6
              rep2-lemma-7
              rep2-lemma-12
              rep2-lemma-46
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-11
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :hints
    (
        ("Goal"
         :use
         (
            vc-1-lemma-105
            vc-1-lemma-108
            vc-1-lemma-114
            vc-1-lemma-106  
            vc-1-lemma-107
            vc-1-lemma-112
            vc-1-lemma-109  
            vc-1-lemma-110
            vc-1-lemma-111
            vc-1-lemma-115
            (:instance
                vc-1-lemma-77
                (a
                    (frame1->a
                        (rep1
                           (-
                               n
                               2
                           )
                           (envir1-init
                               1
                           )
                           (frame1-init
                               1
                               0
                               0
                               a
                           )
                        )
                    )
                )
                (i
                    (-
                        n
                        1
                    )
                )
            )
                    (:instance
                         rep2-lemma-6
                         (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                     (:instance
                         rep2-lemma-7
                         (iteration
                            (-
                               n
                               1
                            )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                    (:instance
                         rep2-lemma-5
                          (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                    (:instance
                         sublist-12
                         (i
                                      (-
                                          n
                                          1
                                      )
                         )
                         (u
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                         )
                     )
                     (:instance
                         sublist-11
                         (i
                             (+
                             (frame2->j
                                 (rep2
                             (-
                                 n
                                 1
                             )
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                                 )
                             )
                             1
                             )
                         )
                         (v
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )  
                         )
                         (u
                            (frame2->a
                                 (rep2
                             (-
                                 n
                                 1
                             )
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                                 )
                             )
                         )
                     )
                     (:instance
                         rep2-lemma-46
                         (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     

                      )
                      (:instance
                          rep1-lemma-10
                          (iteration (- n 2))
                          (env1
                                           (envir1-init
                                               1
                                           )
                          )
                          (fr1
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                          )
                      )
                     (:instance
                          rep1-lemma-10
                          (iteration (- n 1))
                          (env1
                                           (envir1-init
                                               1
                                           )
                          )
                          (fr1
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                          )
                      )
              
         )
         :do-not-induct t
        )
    )
)

(defrule vc-1-lemma-117
    (implies
        (and
            (natp n)
            (< 1 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
            (permutation
                0
                (- n 2)
                a0
                (frame1->a
                    (rep1
                        (-
                            n
                            2
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
            (ordered
                0
                (- n 2)
                (frame1->a
                    (rep1
                        (-
                            n
                            2
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
        )
            (ordered
                0
                (- n 1)
                (frame1->a
                    (rep1
                        (-
                            n
                            1
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              vc-1-lemma-110
              vc-1-lemma-111
              vc-1-lemma-112
              vc-1-lemma-113
              vc-1-lemma-114
              vc-1-lemma-115
              vc-1-lemma-116
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep1-lemma-10
              rep1-lemma-14
              rep2
              rep2-lemma-5
              rep2-lemma-6
              rep2-lemma-7
              rep2-lemma-12
              rep2-lemma-46
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-11
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :hints
    (
        ("Goal"
         :use
         (
            vc-1-lemma-105
            vc-1-lemma-108
            vc-1-lemma-114
            vc-1-lemma-106  
            vc-1-lemma-107
            vc-1-lemma-112
            vc-1-lemma-109  
            vc-1-lemma-110
            vc-1-lemma-111
            vc-1-lemma-115
            (:instance
                vc-1-lemma-102
                (a
                    (frame1->a
                        (rep1
                           (-
                               n
                               2
                           )
                           (envir1-init
                               1
                           )
                           (frame1-init
                               1
                               0
                               0
                               a
                           )
                        )
                    )
                )
                (i
                    (-
                        n
                        1
                    )
                )
            )
                    (:instance
                         rep2-lemma-6
                         (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                     (:instance
                         rep2-lemma-7
                         (iteration
                            (-
                               n
                               1
                            )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                    (:instance
                         rep2-lemma-5
                          (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     
                     )
                    (:instance
                         sublist-12
                         (i
                                      (-
                                          n
                                          1
                                      )
                         )
                         (u
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                         )
                     )
                     (:instance
                         sublist-11
                         (i
                             (+
                             (frame2->j
                                 (rep2
                             (-
                                 n
                                 1
                             )
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                                 )
                             )
                             1
                             )
                         )
                         (v
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )  
                         )
                         (u
                            (frame2->a
                                 (rep2
                             (-
                                 n
                                 1
                             )
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                                 )
                             )
                         )
                     )
                     (:instance
                         rep2-lemma-46
                         (iteration
                             (-
                                 n
                                 1
                             )
                         )
                         (env2
                              (envir2-init
                                  (-
                                      n
                                      1
                                  )
                                  (nth
                                      (-
                                          n
                                          1
                                      )
                                      (frame1->a
                                          (rep1
                                               (- n 2)
                                               (envir1-init
                                                   1
                                               )
                                               (frame1-init
                                                   1
                                                   0
                                                   0
                                                   a
                                               )
                                          )
                                      )
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      n
                                      2
                                  )
                                  (frame1->a
                                      (rep1
                                           (- n 2)
                                           (envir1-init
                                               1
                                           )
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                                      )
                                  )
                              )
                          )     

                      )
                      (:instance
                          rep1-lemma-10
                          (iteration (- n 2))
                          (env1
                                           (envir1-init
                                               1
                                           )
                          )
                          (fr1
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                          )
                      )
                     (:instance
                          rep1-lemma-10
                          (iteration (- n 1))
                          (env1
                                           (envir1-init
                                               1
                                           )
                          )
                          (fr1
                                           (frame1-init
                                               1
                                               0
                                               0
                                               a
                                           )
                          )
                      )
              
         )
         :do-not-induct t
        )
    )
)

(defrule vc-1
    (implies
        (and
            (natp n)
            (< 0 n)
            (integer-listp a0)
            (integer-listp a)
            (equal a0 a)
            (<= n (len a0))
        )
        (and
            (permutation
                0
                (- n 1)
                a0
                (frame1->a
                    (rep1
                        (-
                            n
                            1
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
            (ordered
                0
                (- n 1)
                (frame1->a
                    (rep1
                        (-
                            n
                            1
                        )
                        (envir1-init
                            1
                        )
                        (frame1-init
                            1
                            0
                            0
                            a
                        )
                    )
                )
            )
        )
    )
    :disable (vc-1-lemma-4
              vc-1-lemma-5
              vc-1-lemma-6
              vc-1-lemma-7
              vc-1-lemma-8
              vc-1-lemma-9
              vc-1-lemma-10
              vc-1-lemma-11
              vc-1-lemma-13
              vc-1-lemma-14
              vc-1-lemma-15
              vc-1-lemma-16
              vc-1-lemma-17
              vc-1-lemma-24
              vc-1-lemma-25
              vc-1-lemma-26
              vc-1-lemma-28
              vc-1-lemma-29
              vc-1-lemma-30
              vc-1-lemma-31
              vc-1-lemma-32
              vc-1-lemma-33
              vc-1-lemma-34
              vc-1-lemma-35
              vc-1-lemma-36
              vc-1-lemma-37
              vc-1-lemma-38
              vc-1-lemma-39
              vc-1-lemma-40
              vc-1-lemma-41
              vc-1-lemma-42
              vc-1-lemma-43
              vc-1-lemma-44
              vc-1-lemma-45
              vc-1-lemma-46
              vc-1-lemma-47  
              vc-1-lemma-48
              vc-1-lemma-49
              vc-1-lemma-50
              vc-1-lemma-51
              vc-1-lemma-52
              vc-1-lemma-53
              vc-1-lemma-54
              vc-1-lemma-55
              vc-1-lemma-56
              vc-1-lemma-57
              vc-1-lemma-58
              vc-1-lemma-59
              vc-1-lemma-60
              vc-1-lemma-61
              vc-1-lemma-62
              vc-1-lemma-63
              vc-1-lemma-64
              vc-1-lemma-65
              vc-1-lemma-66
              vc-1-lemma-67
              vc-1-lemma-68
              vc-1-lemma-69
              vc-1-lemma-70
              vc-1-lemma-71
              vc-1-lemma-72
              vc-1-lemma-73
              vc-1-lemma-74
              vc-1-lemma-75
              vc-1-lemma-76
              vc-1-lemma-77
              vc-1-lemma-78
              vc-1-lemma-79
              vc-1-lemma-80
              vc-1-lemma-81
              vc-1-lemma-82
              vc-1-lemma-83
              vc-1-lemma-84
              vc-1-lemma-85
              vc-1-lemma-86
              vc-1-lemma-87
              vc-1-lemma-88
              vc-1-lemma-89
              vc-1-lemma-90
              vc-1-lemma-91
              vc-1-lemma-92
              vc-1-lemma-93
              vc-1-lemma-94
              vc-1-lemma-95
              vc-1-lemma-96
              vc-1-lemma-97
              vc-1-lemma-98
              vc-1-lemma-99
              vc-1-lemma-100
              vc-1-lemma-101
              vc-1-lemma-102
              vc-1-lemma-103
              vc-1-lemma-105
              vc-1-lemma-106
              vc-1-lemma-107
              vc-1-lemma-108
              vc-1-lemma-109
              vc-1-lemma-110
              vc-1-lemma-111
              vc-1-lemma-112
              vc-1-lemma-113
              vc-1-lemma-114
              vc-1-lemma-115
              vc-1-lemma-116
              vc-1-lemma-117
              rep1-lemma-5
              rep1-lemma-8
              rep1-lemma-9
              rep1-lemma-10
              rep1-lemma-14
              rep2
              rep2-lemma-5
              rep2-lemma-6
              rep2-lemma-7
              rep2-lemma-12
              rep2-lemma-46
              sublist
              sublist-1
              sublist-2
              sublist-7
              sublist-9
              sublist-11
              sublist-12
              sublist-13
              sublist-14
              nth
              update-nth
              nth-update-nth
              nth-of-update-nth-diff
              nth-of-update-nth-same
              permutation
              permutation-3
              permutation-5
              permutation-6
              frame1
              (:type-prescription frame1)
              (:executable-counterpart frame1)
              frame1-fix
              frame1-init
              (:type-prescription frame1-init)
              (:executable-counterpart frame1-init)
              envir1
              (:type-prescription envir1)
              (:executable-counterpart envir1)
              envir1-fix
              envir1-init
              (:type-prescription envir1-init)
              (:executable-counterpart envir1-init)
              equal-ranges
              equal-ranges-sublists
              equal-ranges-sublists-1
              ordered
              ordered-1
              ordered-3)
    :hints
    (
        ("Goal"
            :induct (dec-induct n)
        )
        ("Subgoal *1/2"
            :cases ((< 1 n))
        )
        ("Subgoal *1/2.2"
            :use (vc-1-lemma-103)
            :do-not-induct t
        )
        ("Subgoal *1/2.1"
            :do-not-induct t
            :use
            (
              vc-1-lemma-116
              vc-1-lemma-117
            )
        )
    )
)
