(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)
(include-book "tools/with-arith5-help" :dir :system)
(local (allow-arith5-help))

(include-book "permut")
(include-book "ordered")
(include-book "range")

(fty::defprod frame2
    ((loop-break booleanp)
     (j integerp)
     (a integer-listp)))

(fty::defprod envir2
    ((upper-bound integerp)
     (k integerp)))

(define frame2-init
    ((j integerp)
     (a integer-listp))
    :returns (fr2 frame2-p)
    (make-frame2
        :loop-break nil
        :j j
        :a a
    )
    ///
    (fty::deffixequiv frame2-init))

(define envir2-init
    ((upper-bound integerp)
     (k integerp))
    :returns (env2 envir2-p)
    (make-envir2
        :upper-bound upper-bound
        :k k
    )
    ///
    (fty::deffixequiv envir2-init))

(define rep2
    ((iteration natp)
     (env2 envir2-p)
     (fr2 frame2-p))
    :measure (nfix iteration)
    :verify-guards nil
    :returns (upd-fr2 frame2-p)
    (b*
        ((iteration (nfix iteration))
         (env2 (envir2-fix env2))
         (fr2 (frame2-fix fr2))        
         ((when (zp iteration)) fr2)
         (fr2 (rep2 (- iteration 1) env2 fr2))
         ((when (frame2->loop-break fr2)) fr2)        
         (fr2
             (if
                 (<=
                     (nth
                         (- (envir2->upper-bound env2) iteration)
                         (frame2->a fr2))
                     (envir2->k env2))
                 (b*
                     ((fr2 (change-frame2 fr2 :loop-break t))
                      ((when t) fr2))
                 fr2)
             fr2))
         ((when (frame2->loop-break fr2)) fr2)
         (fr2 (change-frame2
                 fr2
                 :a
                 (update-nth
                     (+ (- (envir2->upper-bound env2) iteration) 1)
                     (nth
                         (- (envir2->upper-bound env2) iteration)
                         (frame2->a fr2))
                     (frame2->a fr2))))
         (fr2 (change-frame2 fr2 :j (- (- (envir2->upper-bound env2) iteration) 1))))
    fr2)
    ///
    (defrule rep2-lemma-1
        (equal
            (envir2-fix (envir2-fix env2))
            (envir2-fix env2)
        )
    )
    (defrule rep2-lemma-2
        (equal
            (frame2-fix (frame2-fix fr2))
            (frame2-fix fr2)
        )
    )
    (defrule rep2-lemma-3
        (equal
            (rep2 iteration (envir2-fix env2) fr2)
            (rep2 iteration env2 fr2)
        )
        :hints (("Goal"
                 :expand
                 ((rep2 iteration (envir2-fix env2) fr2)
                 (rep2 iteration env2 fr2))))
        :use rep2-lemma-1
        :do-not-induct t
    )
    (defrule rep2-lemma-4
        (equal
            (rep2 iteration env2 (frame2-fix fr2))
            (rep2 iteration env2 fr2)
        )
        :hints (("Goal"
                 :expand
                 ((rep2 iteration env2 (frame2-fix fr2))
                 (rep2 iteration env2 fr2))))
        :use rep2-lemma-2
        :do-not-induct t
    )
    (fty::deffixequiv rep2)
    (defrule rep2-lemma-5
        (integer-listp (frame2->a (rep2 iteration env2 fr2)))
        :induct (dec-induct iteration)
    )
    (defrule rep2-lemma-6
        (implies
            (< (frame2->j fr2) (envir2->upper-bound env2))
            (<= (frame2->j (rep2 iteration env2 fr2)) (- (envir2->upper-bound env2) 1))
        )
        :induct (dec-induct iteration)
        :hints (("Subgoal *1/2"
                 :expand ((rep2 iteration env2 fr2))
                 :do-not-induct t))
    )
    (defrule rep2-lemma-7
        (implies
            (and
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (< (frame2->j fr2) (envir2->upper-bound env2))
            )
            (equal
                (len (frame2->a (rep2 iteration env2 fr2)))
                (len (frame2->a fr2))
            )
        )
        :induct (dec-induct iteration)
        :hints (("Subgoal *1/2"
                 :expand ((rep2 iteration env2 fr2)
                          (len (frame2->a (rep2 (- iteration 1) env2 fr2))))
                 :do-not-induct t))
    )
    (defrule rep2-lemma-8
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (frame2->loop-break (rep2 iteration env2 fr2))
            )
            (equal
                (frame2->j (rep2 (- iteration 1) env2 fr2))
                (frame2->j (rep2 iteration env2 fr2))
            )
        )
        :do-not-induct t
        :hints (("Goal"
                 :expand ((rep2 iteration env2 fr2))))
    )
    (defrule rep2-lemma-9
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
            )
            (equal
                (frame2->j (rep2 iteration env2 fr2))
                (- (- (envir2->upper-bound env2) iteration) 1)
            )
        )
        :do-not-induct t
        :hints (("Goal"
                 :expand ((rep2 iteration env2 fr2))))
    )
    (defrule rep2-lemma-10
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (<= index (frame2->j (rep2 iteration env2 fr2)))
            )
            (<= index (- (- (envir2->upper-bound env2) iteration) 1))
        )
        :use rep2-lemma-9
    )
    (defrule rep2-lemma-11
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= index (- (- (envir2->upper-bound env2) iteration) 1))
            )
            (not
                (equal
                    index
                    (+ (- (envir2->upper-bound env2) iteration) 1)
                )
            )
        )
    )
    (defrule rep2-lemma-12
        (implies
            (not
                (equal
                    index
                    (+ (- (envir2->upper-bound env2) iteration) 1)
                )
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (update-nth
                        (+ (- (envir2->upper-bound env2) iteration) 1)
                        (nth
                            (- (envir2->upper-bound env2) iteration)
                            (frame2->a (rep2 (- iteration 1) env2 fr2))
                        )
                        (frame2->a (rep2 (- iteration 1) env2 fr2))
                    )
                )
            )
        )
        :use ((:instance nth-of-update-nth-diff
               (n1 index)
               (n2 (+ (- (envir2->upper-bound env2) iteration) 1))
               (v (nth (- (envir2->upper-bound env2) iteration)
                       (frame2->a (rep2 (- iteration 1) env2 fr2))))
               (x (frame2->a (rep2 (- iteration 1) env2 fr2)))
              ))
    )
    (defrule rep2-lemma-13
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not
                    (equal
                        index
                        (+ (- (envir2->upper-bound env2) iteration) 1)
                    )
                )
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (update-nth
                        (+ (- (envir2->upper-bound env2) iteration) 1)
                        (nth
                            (- (envir2->upper-bound env2) iteration)
                            (frame2->a (rep2 (- iteration 1) env2 fr2))
                        )
                        (frame2->a (rep2 (- iteration 1) env2 fr2))
                    )
                )
            )
        )
        :use (rep2-lemma-11
              rep2-lemma-12)
    )
    (defrule rep2-lemma-14
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (natp (+ (- (envir2->upper-bound env2) iteration) 1))
        )
        :disable (rep2-lemma-12 rep2-lemma-13)
    )
    (defrule rep2-lemma-15
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integerp
                (nth
                    (- (envir2->upper-bound env2) iteration)
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
            )
        )
        :disable (rep2-lemma-12 rep2-lemma-13)
        :use ((:instance 
               sublist-12
               (i (- (envir2->upper-bound env2) iteration))
               (u (frame2->a (rep2 (- iteration 1) env2 fr2)))))
    )
    (defrule rep2-lemma-16
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (frame2->a (rep2 (- iteration 1) env2 fr2))
            )
        )
        :disable (rep2-lemma-12 rep2-lemma-13)
    )
    (defrule rep2-lemma-17
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (<
                (+ (- (envir2->upper-bound env2) iteration) 1)
                (len (frame2->a (rep2 (- iteration 1) env2 fr2)))
            )
        )
        :disable (rep2-lemma-12 rep2-lemma-13)
    )
    (defrule rep2-lemma-18
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (and
                (natp (+ (- (envir2->upper-bound env2) iteration) 1))
                (integerp
                    (nth
                        (- (envir2->upper-bound env2) iteration)
                        (frame2->a (rep2 (- iteration 1) env2 fr2))
                    )
                )
                (integer-listp
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (<
                    (+ (- (envir2->upper-bound env2) iteration) 1)
                    (len (frame2->a (rep2 (- iteration 1) env2 fr2)))
                )
            )
        )
        :do-not-induct t
        :disable (rep2-lemma-12 rep2-lemma-13)
        :use (rep2-lemma-14 rep2-lemma-15 rep2-lemma-16 rep2-lemma-17)
    )
    (defrule rep2-lemma-19
        (implies
            (and
                (natp iteration)
                (not (zp iteration))
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (update-nth
                    (+ (- (envir2->upper-bound env2) iteration) 1)
                    (nth
                        (- (envir2->upper-bound env2) iteration)
                        (frame2->a (rep2 (- iteration 1) env2 fr2))
                    )
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
            )
        )
        :disable (rep2-lemma-12 rep2-lemma-13)
        :use ((:instance
               sublist-11
               (i (+ (- (envir2->upper-bound env2) iteration) 1))
               (v
                    (nth
                        (- (envir2->upper-bound env2) iteration)
                        (frame2->a (rep2 (- iteration 1) env2 fr2))
                    )
               )
               (u (frame2->a (rep2 (- iteration 1) env2 fr2)))))
    )
    (defrule rep2-lemma-20
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (not
                    (equal
                        index
                        (+ (- (envir2->upper-bound env2) iteration) 1)
                    )
                )
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5)
        :use rep2-lemma-13
        :hints (("Goal"
                 :expand ((rep2 iteration env2 fr2))
                 :cases ((not (zp iteration)))
               )
               ("Subgoal 1" :use rep2-lemma-19))
    )
    (defrule rep2-lemma-21
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (frame2->loop-break (rep2 iteration env2 fr2))
                (not
                    (equal
                        index
                        (+ (- (envir2->upper-bound env2) iteration) 1)
                    )
                )
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5)
        :use rep2-lemma-13
        :hints (("Goal"
                 :expand ((rep2 iteration env2 fr2))
                 :cases ((not (zp iteration)))
               )
               ("Subgoal 1" :use rep2-lemma-19))
    )
    (defrule rep2-lemma-22
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not
                    (equal
                        index
                        (+ (- (envir2->upper-bound env2) iteration) 1)
                    )
                )
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5)
        :hints (("Goal"
                 :cases ((frame2->loop-break (rep2 iteration env2 fr2))))
                 ("Subgoal 2"
                  :use rep2-lemma-20)
                 ("Subgoal 1"
                  :use rep2-lemma-21)
               )
    )
    (defrule rep2-lemma-23
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (<=
                (- (- (envir2->upper-bound env2) iteration) 1)
                (frame2->j (rep2 iteration env2 fr2))
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-6 rep2-lemma-9 rep2-lemma-5 rep2-lemma-10
                  rep2-lemma-22)
        :hints (("Subgoal *1/2"
                 :expand ((rep2 iteration env2 fr2))
                 :do-not-induct t))
    )
    (defrule rep2-lemma-24
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= index (+ (frame2->j (rep2 iteration env2 fr2)) 1))
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22)
        :hints (("Goal"
                 :cases ((frame2->loop-break (rep2 iteration env2 fr2))))
                 ("Subgoal 2"
                  :use (rep2-lemma-9 rep2-lemma-22))
                 ("Subgoal 1"
                  :expand ((rep2 iteration env2 fr2)))
               )
    )
    (defrule rep2-lemma-25
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= index (+ (frame2->j (rep2 iteration env2 fr2)) 1))
            )
            (<= index (+ (frame2->j (rep2 (- iteration 1) env2 fr2)) 1))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22)
        :hints (("Goal"
                 :cases ((frame2->loop-break (rep2 (- iteration 1) env2 fr2))))
                 ("Subgoal 2"
                  :expand ((rep2 iteration env2 fr2))
                  :use ((:instance rep2-lemma-9 (iteration (- iteration 1)))))
                 ("Subgoal 1"
                  :expand ((rep2 iteration env2 fr2)))
               )
    )
    (defrule rep2-lemma-26
        (implies
            (and
                (not (zp iteration))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= index (+ (frame2->j (rep2 iteration env2 fr2)) 1))
            )
            (and
                (natp (- iteration 1))
                (natp index)
                (<= (- iteration 1) (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= index (+ (frame2->j (rep2 (- iteration 1) env2 fr2)) 1))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-25)
        :use rep2-lemma-25
    )
    (defrule rep2-lemma-27
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= index (+ (frame2->j (rep2 iteration env2 fr2)) 1))
            )
            (equal
                (nth
                    index
                    (frame2->a fr2)
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26)
        :hints (("Subgoal *1/2"
                 :use (rep2-lemma-24 rep2-lemma-26)
                 :do-not-induct t))
    )
    (defrule rep2-lemma-28
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (= index (+ (frame2->j (rep2 iteration env2 fr2)) 1))
            )
            (equal
                (nth
                    index
                    (frame2->a fr2)
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27)
        :use rep2-lemma-27
    )
    (defrule rep2-lemma-29
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (not (frame2->loop-break (rep2 (- iteration 1) env2 fr2)))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25)
        :hints (("Goal" :expand (rep2 iteration env2 fr2)))
    )
    (defrule rep2-lemma-30
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (frame2->j (rep2 (- iteration 1) env2 fr2))
                (- (envir2->upper-bound env2) iteration)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25)
        :use (rep2-lemma-29 (:instance rep2-lemma-9 (iteration (- iteration 1))))
    )
    (defrule rep2-lemma-31
        (iff
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (>= index (+ (frame2->j (rep2 iteration env2 fr2)) 2))
            )
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (>= index (+ (frame2->j (rep2 (- iteration 1) env2 fr2)) 1))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30)
        :use (rep2-lemma-9 rep2-lemma-30)
    )
    (defrule rep2-lemma-32
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (> index (+ (frame2->j (rep2 (- iteration 1) env2 fr2)) 1))               
            )
            (> index (+ (- (envir2->upper-bound env2) iteration) 1))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30)
        :use rep2-lemma-30
    )
    (defrule rep2-lemma-33
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (> index (+ (- (envir2->upper-bound env2) iteration) 1))
            )
            (not
                (equal
                    index
                    (+ (- (envir2->upper-bound env2) iteration) 1)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32)
        :use rep2-lemma-32
    )
   (defrule rep2-lemma-34
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (> index (+ (frame2->j (rep2 (- iteration 1) env2 fr2)) 1))               
            )
            (not
                (equal
                    index
                    (+ (- (envir2->upper-bound env2) iteration) 1)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33)
        :use (rep2-lemma-32 rep2-lemma-33)
    )
    (defrule rep2-lemma-35
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (> index (+ (- (envir2->upper-bound env2) iteration) 1))
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (update-nth
                        (+ (- (envir2->upper-bound env2) iteration) 1)
                        (nth
                            (- (envir2->upper-bound env2) iteration)
                            (frame2->a (rep2 (- iteration 1) env2 fr2))
                        )
                        (frame2->a (rep2 (- iteration 1) env2 fr2))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33)
        :use (rep2-lemma-33
              rep2-lemma-12)
    )
    (defrule rep2-lemma-36
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (> index (+ (frame2->j (rep2 (- iteration 1) env2 fr2)) 1))               
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (update-nth
                        (+ (- (envir2->upper-bound env2) iteration) 1)
                        (nth
                            (- (envir2->upper-bound env2) iteration)
                            (frame2->a (rep2 (- iteration 1) env2 fr2))
                        )
                        (frame2->a (rep2 (- iteration 1) env2 fr2))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35)
        :use (rep2-lemma-35
              rep2-lemma-32)
    )
    (defrule rep2-lemma-37
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (nth
                    (- (envir2->upper-bound env2) iteration)
                    (frame2->a fr2)
                )
                (nth
                    (- (envir2->upper-bound env2) iteration)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28)
        :use (rep2-lemma-9
              (:instance rep2-lemma-28 (index (- (envir2->upper-bound env2) iteration))))
    )
    (defrule rep2-lemma-38
        (implies
            (and
                (not (zp iteration))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (nth
                    (- (envir2->upper-bound env2) iteration)
                    (frame2->a fr2)
                )
                (nth
                    (- (envir2->upper-bound env2) iteration)
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37)
        :use (rep2-lemma-37
              (:instance rep2-lemma-20 (index (- (envir2->upper-bound env2) iteration))))
    )
    (defrule rep2-lemma-39
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (<= index (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (>= index (+ (frame2->j (rep2 iteration env2 fr2)) 2))
            )
            (equal
                (nth
                    (- index 1)
                    (frame2->a fr2)
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2)))))
                 ("Subgoal *1/2.2"
                 :use rep2-lemma-8
                 :expand (rep2 iteration env2 fr2)
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep2-lemma-31
                 :cases ((> index (+ (frame2->j (rep2 (- iteration 1) env2 fr2)) 1))
                         (= index (+ (frame2->j (rep2 (- iteration 1) env2 fr2)) 1)))
                 :do-not-induct t)
                 ("Subgoal *1/2.1.2"
                 :use (rep2-lemma-19 rep2-lemma-36)
                 :expand (rep2 iteration env2 fr2)
                 :do-not-induct t)
                 ("Subgoal *1/2.1.1"
                 :use (rep2-lemma-9 rep2-lemma-19 rep2-lemma-30 rep2-lemma-38)
                 :expand (rep2 iteration env2 fr2)
                 :do-not-induct t))
    )
    (defrule rep2-lemma-40
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (natp (+ (+ (frame2->j (rep2 iteration env2 fr2)) 1) 1))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2)))))
                 ("Subgoal *1/2.2"
                 :use rep2-lemma-8
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep2-lemma-9
                 :do-not-induct t))
    )
    (defrule rep2-lemma-41
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (< (+ (frame2->j (rep2 iteration env2 fr2)) 1) (len (frame2->a fr2)))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2)))))
                 ("Subgoal *1/2.2"
                 :use rep2-lemma-8
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep2-lemma-9
                 :do-not-induct t))
    )
    (defrule rep2-lemma-42
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (< (+ (frame2->j (rep2 iteration env2 fr2)) 1) (len (frame2->a (rep2 iteration env2 fr2))))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41)
        :use (rep2-lemma-7 rep2-lemma-41)
    )
    (defrule rep2-lemma-43
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= bound (+ (frame2->j (rep2 iteration env2 fr2)) 1))
            )
            (equal-ranges
                bound
                (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                bound
                (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                (frame2->a fr2)
                (frame2->a (rep2 iteration env2 fr2))
            )
        )
        :induct (inc-induct bound (+ (+ (frame2->j (rep2 iteration env2 fr2)) 1) 1))
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 equal-ranges-sublists
                  equal-ranges-sublists-1)
        :enable (equal-ranges)
        :hints (("Subgoal *1/1"
                 :cases ((<= (+ bound 1) (+ (frame2->j (rep2 iteration env2 fr2)) 1))))
                ("Subgoal *1/1.2"
                 :do-not-induct t
                 :use (rep2-lemma-41 rep2-lemma-42
                      (:instance rep2-lemma-27 (index bound)))
                 :expand
                     (equal-ranges
                         bound
                         (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                         bound
                         (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                         (frame2->a fr2)
                         (frame2->a (rep2 iteration env2 fr2))
                     )
                )
                ("Subgoal *1/1.1"
                 :do-not-induct t
                 :use (rep2-lemma-41 rep2-lemma-42
                      (:instance rep2-lemma-27 (index bound)))
                 :expand
                     (equal-ranges
                         bound
                         (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                         bound
                         (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                         (frame2->a fr2)
                         (frame2->a (rep2 iteration env2 fr2))
                     )
                ))
    )
    (defrule rep2-lemma-44
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (< 0 (+ (+ (frame2->j (rep2 iteration env2 fr2)) 1) 1))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 equal-ranges-sublists equal-ranges-sublists-1)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2)))))
                 ("Subgoal *1/2.2"
                 :use rep2-lemma-8
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep2-lemma-9
                 :do-not-induct t))
    )
    (defrule rep2-lemma-45
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= (+ (frame2->j (rep2 iteration env2 fr2)) 2) bound)
                (<= bound (envir2->upper-bound env2))
            )
            (equal-ranges
                (- bound 1)
                (- (envir2->upper-bound env2) 1)
                bound
                (envir2->upper-bound env2)
                (frame2->a fr2)
                (frame2->a (rep2 iteration env2 fr2))
            )
        )
        :induct (inc-induct bound (+ (envir2->upper-bound env2) 1))
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1)
        :enable (equal-ranges)
        :hints (("Subgoal *1/1"
                 :cases ((<= (+ bound 1) (envir2->upper-bound env2))))
                ("Subgoal *1/1.2"
                 :do-not-induct t
                 :use (rep2-lemma-7 rep2-lemma-44
                      (:instance rep2-lemma-39 (index bound)))
                 :expand
                     (equal-ranges
                         (- bound 1)
                         (- (envir2->upper-bound env2) 1)
                         bound
                         (envir2->upper-bound env2)
                         (frame2->a fr2)
                         (frame2->a (rep2 iteration env2 fr2))
                     )
                )
                ("Subgoal *1/1.1"
                 :do-not-induct t
                 :use (rep2-lemma-7 rep2-lemma-44
                      (:instance rep2-lemma-39 (index bound)))
                 :expand
                     (equal-ranges
                         (- bound 1)
                         (- (envir2->upper-bound env2) 1)
                         bound
                         (envir2->upper-bound env2)
                         (frame2->a fr2)
                         (frame2->a (rep2 iteration env2 fr2))
                     )
                ))
        )
    )
    (defrule rep2-lemma-46
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (natp (+ (frame2->j (rep2 iteration env2 fr2)) 1))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2)))))
                 ("Subgoal *1/2.2"
                 :use rep2-lemma-8
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep2-lemma-9
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :expand (rep2 0 env2 fr2)
                 :do-not-induct t))
    )
    (defrule rep2-lemma-47
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= bound (+ (frame2->j (rep2 iteration env2 fr2)) 1))
            )
            (equal
                (sublist 
                    bound
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (sublist
                    bound
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-46)
        :use (rep2-lemma-41 rep2-lemma-42 rep2-lemma-43 rep2-lemma-46
               (:instance equal-ranges-sublists
                (i1 bound)
                (i2 bound)
                (j1 (+ (frame2->j (rep2 iteration env2 fr2)) 1))
                (j2 (+ (frame2->j (rep2 iteration env2 fr2)) 1))
                (l1 (frame2->a fr2))
                (l2 (frame2->a (rep2 iteration env2 fr2)))))
    )
    (defrule rep2-lemma-48
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= (+ (frame2->j (rep2 iteration env2 fr2)) 2) bound)
                (<= bound (envir2->upper-bound env2))
            )
            (natp (- bound 1))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46)
        :use (rep2-lemma-44 rep2-lemma-46)
    )
    (defrule rep2-lemma-49
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= (+ (frame2->j (rep2 iteration env2 fr2)) 2) bound)
                (<= bound (envir2->upper-bound env2))
            )
            (natp (- (envir2->upper-bound env2) 1))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-48)
        :use (rep2-lemma-44 rep2-lemma-46 rep2-lemma-48)
    )
    (defrule rep2-lemma-50
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= (+ (frame2->j (rep2 iteration env2 fr2)) 2) bound)
                (<= bound (envir2->upper-bound env2))
            )
            (natp (envir2->upper-bound env2))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-48
                  rep2-lemma-48)
        :use (rep2-lemma-44 rep2-lemma-46 rep2-lemma-48 rep2-lemma-48)
    )
    (defrule rep2-lemma-51
        (implies
            (and
                (natp iteration)
                (natp bound)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= (+ (frame2->j (rep2 iteration env2 fr2)) 2) bound)
                (<= bound (envir2->upper-bound env2))
            )
            (equal
                (sublist 
                    (- bound 1)
                    (- (envir2->upper-bound env2) 1)
                    (frame2->a fr2)
                )
                (sublist
                    bound
                    (envir2->upper-bound env2)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50)
        :use (rep2-lemma-7 rep2-lemma-41 rep2-lemma-42 rep2-lemma-44 rep2-lemma-45 rep2-lemma-46
              rep2-lemma-48 rep2-lemma-49 rep2-lemma-50
               (:instance equal-ranges-sublists
                (i1 (- bound 1))
                (i2 bound)
                (j1 (- (envir2->upper-bound env2) 1))
                (j2 (envir2->upper-bound env2))
                (l1 (frame2->a fr2))
                (l2 (frame2->a (rep2 iteration env2 fr2)))))
    )
    (defrule rep2-lemma-52
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= 0 (+ (frame2->j (rep2 iteration env2 fr2)) 1))
            )
            (equal
                (sublist 
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51)
        :use (rep2-lemma-46
               (:instance rep2-lemma-47 (bound 0)))
    )
    (defrule rep2-lemma-53
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<= (+ (frame2->j (rep2 iteration env2 fr2)) 2) (envir2->upper-bound env2))
            )
            (equal
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (- (envir2->upper-bound env2) 1)
                    (frame2->a fr2)
                )
                (sublist
                    (+ (frame2->j (rep2 iteration env2 fr2)) 2)
                    (envir2->upper-bound env2)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52)
        :use (rep2-lemma-40
               (:instance rep2-lemma-51
                (bound (+ (frame2->j (rep2 iteration env2 fr2)) 2))))
    )
    (defrule rep2-lemma-54
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (sublist 
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53)
         :enable (sublist)
         :hints (("Goal" :cases ((<= 0 (+ (frame2->j (rep2 iteration env2 fr2)) 1))))
                 ("Subgoal 2" :expand
                     (
                         (sublist 
                             0
                             (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                             (frame2->a fr2)
                         )
                         (sublist 
                             0
                             (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                             (frame2->a (rep2 iteration env2 fr2))
                         )
                     )
                 )
                 ("Subgoal 1" :use rep2-lemma-52))
    )
    (defrule rep2-lemma-55
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))              
            )
            (equal
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (- (envir2->upper-bound env2) 1)
                    (frame2->a fr2)
                )
                (sublist
                    (+ (frame2->j (rep2 iteration env2 fr2)) 2)
                    (envir2->upper-bound env2)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54)
         :enable (sublist)
         :hints (("Goal"
                 :cases ((<=
                            (+ (frame2->j (rep2 iteration env2 fr2)) 2)
                            (envir2->upper-bound env2))))
                 ("Subgoal 2" :expand
                     (
                         (sublist 
                             (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                             (- (envir2->upper-bound env2) 1)
                             (frame2->a fr2)
                         )
                         (sublist 
                             (+ (frame2->j (rep2 iteration env2 fr2)) 2)
                             (envir2->upper-bound env2)
                             (frame2->a (rep2 iteration env2 fr2))
                         )
                     )
                 )
                 ("Subgoal 1" :use rep2-lemma-53))
    )
    (defrule rep2-lemma-56
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (sublist
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55)
        :use (rep2-lemma-46
               (:instance rep2-lemma-47
                (bound (+ (frame2->j (rep2 iteration env2 fr2)) 1))))
    )
    (defrule rep2-lemma-57
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (sublist 
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6)
        :use (rep2-lemma-5 rep2-lemma-46
              (:instance sublist-6
               (i 0)
               (j (+ (frame2->j (rep2 iteration env2 fr2)) 1))
               (u (frame2->a fr2))))
    )
    (defrule rep2-lemma-58
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (sublist 
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57)
        :use (rep2-lemma-5 rep2-lemma-46
              (:instance sublist-6
               (i 0)
               (j (+ (frame2->j (rep2 iteration env2 fr2)) 1))
               (u (frame2->a (rep2 iteration env2 fr2)))))
    )
    (defrule rep2-lemma-59
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (element-list-equiv
                (sublist 
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58)
        :use (rep2-lemma-54 rep2-lemma-57 rep2-lemma-58)
    )
    (defrule rep2-lemma-60
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59)
        :use (rep2-lemma-5 rep2-lemma-46
              (:instance sublist-6
               (i (+ (frame2->j (rep2 iteration env2 fr2)) 1))
               (j (+ (frame2->j (rep2 iteration env2 fr2)) 1))
               (u (frame2->a fr2))))
    )
    (defrule rep2-lemma-61
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60)
        :use (rep2-lemma-5 rep2-lemma-46
              (:instance sublist-6
               (i (+ (frame2->j (rep2 iteration env2 fr2)) 1))
               (j (+ (frame2->j (rep2 iteration env2 fr2)) 1))
               (u (frame2->a (rep2 iteration env2 fr2)))))
    )
    (defrule rep2-lemma-62
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (element-list-equiv
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (sublist
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61)
        :use (rep2-lemma-56 rep2-lemma-60 rep2-lemma-61)
    )
    (defrule rep2-lemma-63
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (- (envir2->upper-bound env2) 1)
                    (frame2->a fr2)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (- (envir2->upper-bound env2) 1))))
                ("Subgoal 2" :use (rep2-lemma-5 rep2-lemma-46)
                    :expand
                    (
                       (integer-listp
                           (sublist 
                               (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                               (- (envir2->upper-bound env2) 1)
                               (frame2->a fr2)
                           )
                       )))
                ("Subgoal 1" :use (rep2-lemma-5 rep2-lemma-46
                    (:instance sublist-6
                        (i (+ (frame2->j (rep2 iteration env2 fr2)) 1))
                        (j (- (envir2->upper-bound env2) 1))
                        (u (frame2->a fr2))
                    ))))
    )
    (defrule rep2-lemma-64
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 2)
                    (envir2->upper-bound env2)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (envir2->upper-bound env2))))
                ("Subgoal 2" :use (rep2-lemma-5 rep2-lemma-40)
                    :expand
                    (
                       (integer-listp
                           (sublist 
                               (+ (frame2->j (rep2 iteration env2 fr2)) 2)
                               (envir2->upper-bound env2)
                               (frame2->a (rep2 iteration env2 fr2))
                           )
                       )))
                ("Subgoal 1" :use (rep2-lemma-5 rep2-lemma-40
                    (:instance sublist-6
                        (i (+ (frame2->j (rep2 iteration env2 fr2)) 2))
                        (j (envir2->upper-bound env2))
                        (u (frame2->a (rep2 iteration env2 fr2)))
                    ))))
    )
    (defrule rep2-lemma-65
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (element-list-equiv
                (sublist 
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (- (envir2->upper-bound env2) 1)
                    (frame2->a fr2)
                )
                (sublist
                    (+ (frame2->j (rep2 iteration env2 fr2)) 2)
                    (envir2->upper-bound env2)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64)
        :use (rep2-lemma-55 rep2-lemma-63 rep2-lemma-64) 
    )
    (defrule rep2-lemma-66
        (implies
            (and
                (natp (frame2->j (rep2 iteration env2 fr2)))
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a fr2)
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a fr2)
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65)
        :hints (("Goal" :use
                  (rep2-lemma-41 rep2-lemma-46
                    (:instance sublist-14
                      (i 0)
                      (j (frame2->j (rep2 iteration env2 fr2)))
                      (k (+ (frame2->j (rep2 iteration env2 fr2)) 1))
                      (u (frame2->a fr2))
                    )
                  )
                )) 
    )
    (defrule rep2-lemma-67
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a fr2)
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a fr2)
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (frame2->j (rep2 iteration env2 fr2)))))
                ("Subgoal 2" :expand
                  (
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a fr2)
                    )
                  )
                )
                ("Subgoal 1" :use (rep2-lemma-66)
                )) 
    )
    (defrule rep2-lemma-68
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (sublist 
                    0
                    (frame2->j (rep2 iteration env2 fr2))
                    (frame2->a fr2)
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (frame2->j (rep2 iteration env2 fr2)))))
                ("Subgoal 2" :use (rep2-lemma-5)
                    :expand
                    (
                       (integer-listp
                           (sublist 
                               0
                               (frame2->j (rep2 iteration env2 fr2))
                               (frame2->a fr2)
                           )
                       )))
                ("Subgoal 1" :use (rep2-lemma-5
                    (:instance sublist-6
                        (i 0)
                        (j (frame2->j (rep2 iteration env2 fr2)))
                        (u (frame2->a fr2))
                    ))))
    )
    (defrule rep2-lemma-69
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (element-list-equiv
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a fr2)
                )
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a fr2)
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a fr2)
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68)
        :use (rep2-lemma-57 rep2-lemma-60 rep2-lemma-67 rep2-lemma-68)
    )
    (defrule rep2-lemma-70
        (implies
            (and
                (natp (frame2->j (rep2 iteration env2 fr2)))
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69)
        :hints (("Goal" :use
                  (rep2-lemma-7 rep2-lemma-41 rep2-lemma-46
                    (:instance sublist-14
                      (i 0)
                      (j (frame2->j (rep2 iteration env2 fr2)))
                      (k (+ (frame2->j (rep2 iteration env2 fr2)) 1))
                      (u (frame2->a (rep2 iteration env2 fr2)))
                    )
                  )
                )) 
    )
    (defrule rep2-lemma-71
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (frame2->j (rep2 iteration env2 fr2)))))
                ("Subgoal 2" :expand
                  (
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                  )
                )
                ("Subgoal 1" :use (rep2-lemma-70)
                )) 
    )
    (defrule rep2-lemma-72
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (integer-listp
                (sublist 
                    0
                    (frame2->j (rep2 iteration env2 fr2))
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist-2 sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71)
        :enable (sublist)
        :hints (("Goal" :cases ((natp (frame2->j (rep2 iteration env2 fr2)))))
                ("Subgoal 2" :use (rep2-lemma-5)
                    :expand
                    (
                       (integer-listp
                           (sublist 
                               0
                               (frame2->j (rep2 iteration env2 fr2))
                               (frame2->a (rep2 iteration env2 fr2))
                           )
                       )))
                ("Subgoal 1" :use (rep2-lemma-5
                    (:instance sublist-6
                        (i 0)
                        (j (frame2->j (rep2 iteration env2 fr2)))
                        (u (frame2->a (rep2 iteration env2 fr2)))
                    ))))
    )
    (defrule rep2-lemma-73
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (element-list-equiv
                (sublist
                    0
                    (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72)
        :use (rep2-lemma-58 rep2-lemma-61 rep2-lemma-71 rep2-lemma-72)
    )
    (defrule rep2-lemma-74
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a fr2)
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a fr2)
                    )
                )
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73)
        :use (rep2-lemma-54 rep2-lemma-67 rep2-lemma-71)
    )
    (defrule rep2-lemma-75
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a fr2)
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a fr2)
                    )
                )
                (append
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a (rep2 iteration env2 fr2))
                    )
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a fr2)
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74)
        :use (rep2-lemma-56 rep2-lemma-60 rep2-lemma-61 rep2-lemma-74
              (:instance element-list-equiv-implies-element-list-equiv-append-2
               (a 
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a (rep2 iteration env2 fr2))
                    )
               )
               (b
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a (rep2 iteration env2 fr2))
                    )
               )
               (b-equiv
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a fr2)
                    )
               )
               ))
    )
    (defrule rep2-lemma-76
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (equal
                (sublist
                    0
                    (frame2->j (rep2 iteration env2 fr2))
                    (frame2->a fr2)
                )
                (sublist
                     0
                     (frame2->j (rep2 iteration env2 fr2))
                     (frame2->a (rep2 iteration env2 fr2))
                 )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75)
        :use (rep2-lemma-68 rep2-lemma-72 rep2-lemma-75
              (:instance equal-of-appends-when-true-listps
               (x1 
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a fr2)
                    )
               )
               (x2 
                    (sublist
                        0
                        (frame2->j (rep2 iteration env2 fr2))
                        (frame2->a (rep2 iteration env2 fr2))
                    )
               )
               (y 
                    (sublist
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (+ (frame2->j (rep2 iteration env2 fr2)) 1)
                        (frame2->a fr2)
                    )
               )
              ))
    )
    (defrule rep2-lemma-77
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (< (envir2->upper-bound env2) index)
                (< index (len (frame2->a fr2)))
            )
            (equal
                (nth
                    index
                    (frame2->a (rep2 (- iteration 1) env2 fr2))
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76)
        :hints (("Goal"
                 :cases ((frame2->loop-break (rep2 iteration env2 fr2))))
                 ("Subgoal 2"
                  :use (rep2-lemma-22))
                 ("Subgoal 1"
                  :expand ((rep2 iteration env2 fr2)))
               )
    )
    (defrule rep2-lemma-78
        (implies
            (and
                (natp iteration)
                (natp index)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (< (envir2->upper-bound env2) index)
                (< index (len (frame2->a fr2)))
            )
            (equal
                (nth
                    index
                    (frame2->a fr2)
                )
                (nth
                    index
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77)
        :hints (("Subgoal *1/2"
                 :use (rep2-lemma-77)
                 :do-not-induct t)
                ("Subgoal *1/1"
                 :expand ((rep2 0 env2 fr2))
                 :do-not-induct t))
    )
    (defrule rep2-lemma-79
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (frame2->loop-break (rep2 iteration env2 fr2))
                (<
                    (envir2->k env2)
                    (nth
                        (+
                            (frame2->j (rep2 (- iteration 1) env2 fr2))
                            2
                        )
                        (frame2->a (rep2 (- iteration 1) env2 fr2))
                    )
                )
            )
            (<
                (envir2->k env2)
                (nth
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78)
        :hints (("Goal"
                 :expand (rep2 iteration env2 fr2)
                 :use (rep2-lemma-8)))
    )
    (defrule rep2-lemma-80
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (<= 0 (- iteration 1))
            )
            (<
                (envir2->k env2)
                (nth
                    (+ (- (envir2->upper-bound env2) iteration) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79)
        :hints (("Goal"
                 :use
                 (
                     rep2-lemma-19
                 )
                 :expand
                 (
                     (rep2 iteration env2 fr2)
                     (rep2 1 env2 fr2)
                     (rep2 0 env2 fr2)
                 )
                 ))
    )
    (defrule rep2-lemma-81
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (<=
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (envir2->upper-bound env2)
                )
            )
            (<
                (envir2->k env2)
                (nth
                    (+ (- (envir2->upper-bound env2) iteration) 1)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80)
        :use (rep2-lemma-9 rep2-lemma-80)
    )
    (defrule rep2-lemma-82
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (<=
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (envir2->upper-bound env2)
                )
            )
            (<
                (envir2->k env2)
                (nth
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81)
        :use (rep2-lemma-9 rep2-lemma-81)
    )
    (defrule rep2-lemma-83
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<=
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (envir2->upper-bound env2)
                )
            )
            (<
                (envir2->k env2)
                (nth
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2)))))
                 ("Subgoal *1/2.2"
                 :use (rep2-lemma-8)
                 :expand (rep2 iteration env2 fr2)
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep2-lemma-82
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :use (rep2-lemma-9)
                 :expand (rep2 0 env2 fr2)
                 :do-not-induct t))
    )
    (defrule rep2-lemma-84
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (<= 0 (- iteration 1))
            )
            (<
                (envir2->k env2)
                (nth
                    (- (envir2->upper-bound env2) iteration)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83)
        :hints (("Goal"
                 :use
                 (
                     rep2-lemma-19
                 )
                 :expand
                 (
                     (rep2 iteration env2 fr2)
                     (rep2 1 env2 fr2)
                     (rep2 0 env2 fr2)
                 )
                 ))
    )
    (defrule rep2-lemma-85
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (<=
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (envir2->upper-bound env2)
                )
            )
            (<
                (envir2->k env2)
                (nth
                    (- (envir2->upper-bound env2) iteration)
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84)
        :use (rep2-lemma-9 rep2-lemma-84)
    )
    (defrule rep2-lemma-86
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
                (<=
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (envir2->upper-bound env2)
                )
            )
            (<
                (envir2->k env2)
                (nth
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        1
                    )
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85)
        :use (rep2-lemma-9 rep2-lemma-85)
    )
    (defrule rep2-lemma-87
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<=
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (envir2->upper-bound env2)
                )
            )
            (<
                (envir2->k env2)
                (nth
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        1
                    )
                    (frame2->a (rep2 iteration env2 fr2))
                )
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86)
        :hints (("Subgoal *1/2"
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2)))))
                 ("Subgoal *1/2.2"
                 :use (rep2-lemma-8)
                 :expand (rep2 iteration env2 fr2)
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use rep2-lemma-86
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :use (rep2-lemma-9)
                 :expand (rep2 0 env2 fr2)
                 :do-not-induct t))
    )
    (defrule rep2-lemma-88
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (frame2->loop-break (rep2 iter env2 fr2))
            )
            (frame2->loop-break (rep2 iteration env2 fr2))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :expand (rep2 iteration env2 fr2)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep2-lemma-89
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (frame2->loop-break (rep2 iter env2 fr2))
            )
            (equal
                (frame2->j (rep2 iter env2 fr2))
                (frame2->j (rep2 iteration env2 fr2))
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use (rep2-lemma-8 rep2-lemma-88)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep2-lemma-90
        (implies
            (and
                (natp iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (<=
                    (+
                        (frame2->j (rep2 iteration env2 fr2))
                        2
                    )
                    (envir2->upper-bound env2)
                )
                (<
                    iteration
                    (-
                        (envir2->upper-bound env2)
                        (frame2->j (rep2 iteration env2 fr2))
                    )
                )
            )
            (not (frame2->loop-break (rep2 iteration env2 fr2)))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89)
        :hints (("Subgoal *1/2"
                 :use
                 (rep2-lemma-8 rep2-lemma-9
                  (:instance rep2-lemma-89
                    (iter iteration)
                    (iteration
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 iteration env2 fr2))
                        )
                    )
                  )
                  (:instance rep2-lemma-9
                    (iteration (- iteration 1))
                  )
                 )
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2))))
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :use (rep2-lemma-9)
                 :expand (rep2 0 env2 fr2)
                 :do-not-induct t))
    )
    (defrule rep2-lemma-91
        (implies
            (and
                (natp iteration)
                (< 0 iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (frame2->loop-break (rep2 iteration env2 fr2))
            )
            (<=
                (frame2->j (rep2 iteration env2 fr2))
                (frame2->j (rep2 (- iteration 1) env2 fr2))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90)
        :use (rep2-lemma-8)
    )
    (defrule rep2-lemma-92
        (implies
            (and
                (natp iteration)
                (< 0 iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
            )
            (<=
                (frame2->j (rep2 iteration env2 fr2))
                (frame2->j (rep2 (- iteration 1) env2 fr2))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91)
        :use (rep2-lemma-9 rep2-lemma-29 rep2-lemma-30)
    )
    (defrule rep2-lemma-93
        (implies
            (and
                (natp iteration)
                (< 0 iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (<=
                (frame2->j (rep2 iteration env2 fr2))
                (frame2->j (rep2 (- iteration 1) env2 fr2))
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92)
        :hints (("Goal"
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2)))))
                 ("Subgoal 2"
                 :use (rep2-lemma-91)
                 :do-not-induct t)
                 ("Subgoal 1"
                 :use (rep2-lemma-92)
                 :do-not-induct t))
    )
    (defrule rep2-lemma-94
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
            )
            (<=
                (frame2->j (rep2 iteration env2 fr2))
                (frame2->j (rep2 iter env2 fr2))
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :use (rep2-lemma-93)
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use (rep2-lemma-93)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep2-lemma-95
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 iteration env2 fr2)))
            )
            (not (frame2->loop-break (rep2 iter env2 fr2)))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :use (rep2-lemma-29)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep2-lemma-96
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break (rep2 (envir2->upper-bound env2) env2 fr2)))
            )
            (equal
                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                -1
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95)
        :use ((:instance rep2-lemma-9
               (iteration (envir2->upper-bound env2))
               ))
    )
   (defrule rep2-lemma-97
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (natp (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2)))
            )
            (frame2->loop-break (rep2 (envir2->upper-bound env2) env2 fr2))
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96)
        :hints (("Goal"
                 :use (rep2-lemma-96)
                 :cases ((not (frame2->loop-break (rep2 (envir2->upper-bound env2) env2 fr2))))))
    )
    (defrule rep2-lemma-98
        (implies
            (and
                (natp iteration)
                (natp (envir2->upper-bound env2))
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (<
                    iteration
                    (-
                        (envir2->upper-bound env2)
                        (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                    )
                )
            )
            (not (frame2->loop-break (rep2 iteration env2 fr2)))
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97)
        :hints (("Subgoal *1/2"
                 :use
                 (rep2-lemma-8
                  (:instance rep2-lemma-6
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-89
                    (iter iteration)
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-9
                    (iteration (- iteration 1))
                  )
                 )
                 :cases ((not (frame2->loop-break (rep2 iteration env2 fr2))))
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :expand (rep2 0 env2 fr2)
                 :do-not-induct t))
    )
    (defrule rep2-lemma-99
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
            )
            (not
                (frame2->loop-break
                    (rep2
                        (-
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            1
                        )
                        env2
                        fr2
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98)
        :use ((:instance rep2-lemma-6
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-46
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-98
                    (iteration
                        (-
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            1
                        )
                    )
                  ))
    )
    (defrule rep2-lemma-100
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (frame2->loop-break
                    (rep2
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                        env2
                        fr2
                    )
                )
            )
            (<=
                (nth
                    (frame2->j
                        (rep2
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            env2
                            fr2
                        )
                    )
                    (frame2->a
                        (rep2
                            (-
                                (-
                                    (envir2->upper-bound env2)
                                    (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                                )
                                1
                            )
                            env2
                            fr2
                        )
                    )
                )
                (envir2->k env2)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99)
        :hints (("Goal" :expand
                 (
                    (rep2
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                        env2
                        fr2
                    )
                 )
                :use
                 (rep2-lemma-99
                  (:instance rep2-lemma-6
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-46
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-9
                        (iteration
                            (-
                                (-
                                    (envir2->upper-bound env2)
                                    (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                                )
                                1
                            )
                        )
                    )
                )
                ))
    )
    (defrule rep2-lemma-101
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (natp (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2)))
                (frame2->loop-break
                    (rep2
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                        env2
                        fr2
                    )
                )
            )
            (<=
                (nth
                    (frame2->j
                        (rep2
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            env2
                            fr2
                        )
                    )
                    (frame2->a
                        (rep2
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            env2
                            fr2
                        )
                    )
                )
                (envir2->k env2)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99 rep2-lemma-100)
        :use (rep2-lemma-99 rep2-lemma-100
              (:instance rep2-lemma-22
               (iteration
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
               )
               (index
                    (frame2->j
                        (rep2
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            env2
                            fr2
                        )
                    )
               ))
                  (:instance rep2-lemma-6
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-8
                    (iteration
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                    )
                  )
                  (:instance rep2-lemma-46
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-9
                        (iteration
                            (-
                                (-
                                    (envir2->upper-bound env2)
                                    (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                                )
                                1
                            )
                        )
                    )
                )
               )
    )
    (defrule rep2-lemma-102
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (natp (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2)))
            )
            (frame2->loop-break
                (rep2
                    (-
                        (envir2->upper-bound env2)
                        (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                    )
                    env2
                    fr2
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99 rep2-lemma-100 rep2-lemma-101)
        :hints (("Goal"
                 :use
                 ((:instance rep2-lemma-6
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-46
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                  (:instance rep2-lemma-9
                    (iteration
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                    )
                  )
                  (:instance rep2-lemma-94
                    (iter
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                    )
                    (iteration
                        (envir2->upper-bound env2)
                    )
                  )
                 )
                 :cases
                 (
                    (frame2->loop-break
                        (rep2
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            env2
                            fr2
                        )
                    )
                 )))
    )
    (defrule rep2-lemma-103
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (natp (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2)))
            )
            (<=
                (nth
                    (frame2->j
                        (rep2
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            env2
                            fr2
                        )
                    )
                    (frame2->a
                        (rep2
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            env2
                            fr2
                        )
                    )
                )
                (envir2->k env2)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99 rep2-lemma-100 rep2-lemma-101
                  rep2-lemma-102)
        :use (rep2-lemma-101 rep2-lemma-102)
    )
   (defrule rep2-lemma-104
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (natp (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2)))
            )
            (equal
                (frame2->j
                    (rep2
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                        env2
                        fr2
                    )
                )
                (frame2->j
                    (rep2
                        (envir2->upper-bound env2)
                        env2
                        fr2
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99 rep2-lemma-100 rep2-lemma-101
                  rep2-lemma-102 rep2-lemma-103)
        :use (rep2-lemma-102
               (:instance rep2-lemma-89
                    (iter
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                    )
                    (iteration
                        (envir2->upper-bound env2)
                    )
               )
               (:instance rep2-lemma-6
                    (iteration
                        (envir2->upper-bound env2)
                    )
               )
               (:instance rep2-lemma-46
                    (iteration
                        (envir2->upper-bound env2)
                    )
               ))
    )
   (defrule rep2-lemma-105
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (natp (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2)))
            )
            (<=
                (nth
                    (frame2->j
                        (rep2
                            (envir2->upper-bound env2)
                            env2
                            fr2
                        )
                    )
                    (frame2->a
                        (rep2
                            (-
                                (envir2->upper-bound env2)
                                (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                            )
                            env2
                            fr2
                        )
                    )
                )
                (envir2->k env2)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99 rep2-lemma-100 rep2-lemma-101
                  rep2-lemma-102 rep2-lemma-103 rep2-lemma-104)
        :use (rep2-lemma-103 rep2-lemma-104)
    )
   (defrule rep2-lemma-106
        (implies
            (and
                (natp iteration)
                (natp iter)
                (<= iter iteration)
                (<= iteration (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (frame2->loop-break (rep2 iter env2 fr2))
            )
            (equal
                (frame2->a (rep2 iter env2 fr2))
                (frame2->a (rep2 iteration env2 fr2))
            )
        )
        :induct (dec-induct iteration)
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99 rep2-lemma-100 rep2-lemma-101
                  rep2-lemma-102 rep2-lemma-103 rep2-lemma-104 rep2-lemma-105)
        :hints (("Subgoal *1/2"
                 :cases ((<= iter (- iteration 1))))
                 ("Subgoal *1/2.2"
                 :do-not-induct t)
                 ("Subgoal *1/2.1"
                 :expand (rep2 iteration env2 fr2)
                 :use (rep2-lemma-88)
                 :do-not-induct t)
                 ("Subgoal *1/1"
                 :do-not-induct t))
    )
    (defrule rep2-lemma-107
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (natp (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2)))
            )
            (equal
                (frame2->a
                    (rep2
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                        env2
                        fr2
                    )
                )
                (frame2->a
                    (rep2
                        (envir2->upper-bound env2)
                        env2
                        fr2
                    )
                )
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99 rep2-lemma-100 rep2-lemma-101
                  rep2-lemma-102 rep2-lemma-103 rep2-lemma-104 rep2-lemma-105 rep2-lemma-106)
        :use (rep2-lemma-102
               (:instance rep2-lemma-106
                    (iter
                        (-
                            (envir2->upper-bound env2)
                            (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2))
                        )
                    )
                    (iteration
                        (envir2->upper-bound env2)
                    )
               )
               (:instance rep2-lemma-6
                    (iteration
                        (envir2->upper-bound env2)
                    )
               )
               (:instance rep2-lemma-46
                    (iteration
                        (envir2->upper-bound env2)
                    )
               ))
    )
    (defrule rep2-lemma-108
        (implies
            (and
                (natp (envir2->upper-bound env2))
                (< (envir2->upper-bound env2) (len (frame2->a fr2)))
                (equal (frame2->j fr2) (- (envir2->upper-bound env2) 1))
                (not (frame2->loop-break fr2))
                (natp (frame2->j (rep2 (envir2->upper-bound env2) env2 fr2)))
            )
            (<=
                (nth
                    (frame2->j
                        (rep2
                            (envir2->upper-bound env2)
                            env2
                            fr2
                        )
                    )
                    (frame2->a
                        (rep2
                            (envir2->upper-bound env2)
                            env2
                            fr2
                        )
                    )
                )
                (envir2->k env2)
            )
        )
        :do-not-induct t
        :disable (nth update-nth nth-of-update-nth-diff rep2-lemma-8 rep2-lemma-12
                  append equal-of-appends-when-true-listps
                  element-list-equiv-implies-element-list-equiv-append-2
                  rep2-lemma-13 rep2-lemma-9 rep2-lemma-5 rep2-lemma-22 rep2-lemma-24
                  rep2-lemma-26 rep2-lemma-27 rep2-lemma-27 sublist-11 sublist-12
                  rep2-lemma-7 rep2-lemma-25 rep2-lemma-30 rep2-lemma-32 rep2-lemma-33
                  rep2-lemma-35 rep2-lemma-19 rep2-lemma-36 rep2-lemma-20 rep2-lemma-21
                  rep2-lemma-18 rep2-lemma-29 rep2-lemma-16 rep2-lemma-28 rep2-lemma-37
                  rep2-lemma-38 rep2-lemma-39 rep2-lemma-40 rep2-lemma-41 rep2-lemma-42
                  rep2-lemma-43 rep2-lemma-44 equal-ranges-sublists equal-ranges-sublists-1
                  equal-ranges sublist sublist-13 rep2-lemma-45 rep2-lemma-46 rep2-lemma-47
                  rep2-lemma-48 rep2-lemma-49 rep2-lemma-50 rep2-lemma-51 rep2-lemma-52
                  rep2-lemma-53 rep2-lemma-54 rep2-lemma-55 rep2-lemma-56 sublist-6
                  rep2-lemma-57 rep2-lemma-58 rep2-lemma-59 rep2-lemma-60 rep2-lemma-61
                  rep2-lemma-62 rep2-lemma-63 rep2-lemma-64 rep2-lemma-65 rep2-lemma-66
                  rep2-lemma-67 rep2-lemma-68 rep2-lemma-69 rep2-lemma-70 rep2-lemma-71
                  rep2-lemma-72 rep2-lemma-73 rep2-lemma-74 rep2-lemma-75 rep2-lemma-76
                  rep2-lemma-77 rep2-lemma-78 rep2-lemma-79 rep2-lemma-80 rep2-lemma-81
                  rep2-lemma-82 rep2-lemma-83 rep2-lemma-84 rep2-lemma-85 rep2-lemma-86
                  rep2-lemma-87 rep2-lemma-88 rep2-lemma-89 rep2-lemma-90 rep2-lemma-91
                  rep2-lemma-92 rep2-lemma-93 rep2-lemma-94 rep2-lemma-95 rep2-lemma-96
                  rep2-lemma-97 rep2-lemma-98 rep2-lemma-99 rep2-lemma-100 rep2-lemma-101
                  rep2-lemma-102 rep2-lemma-103 rep2-lemma-104 rep2-lemma-105 rep2-lemma-106
                  rep2-lemma-107)
        :use (rep2-lemma-105 rep2-lemma-107)
    )
)

