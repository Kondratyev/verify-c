(in-package "ACL2")

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)
(include-book "tools/with-arith5-help" :dir :system)
(local (allow-arith5-help))

(include-book "permut")
(include-book "ordered")
(include-book "range")
(include-book "rep2")

(fty::defprod frame1
    ((loop-break booleanp)
     (i integerp)
     (j integerp)
     (k integerp)
     (a integer-listp)))

(fty::defprod envir1
    ((lower-bound integerp)))

(define frame1-init
    ((i integerp)
     (j integerp)
     (k integerp)
     (a integer-listp))
    :returns (fr1 frame1-p)
    (make-frame1
        :loop-break nil
        :i i
        :j j
        :k k
        :a a
    )
    ///
    (fty::deffixequiv frame1-init))

(define envir1-init
    ((lower-bound integerp))
    :returns (env1 envir1-p)
    (make-envir1
        :lower-bound lower-bound
    )
    ///
    (fty::deffixequiv envir1-init))

(define rep1
    ((iteration natp)
     (env1 envir1-p)
     (fr1 frame1-p))
    :measure (nfix iteration)
    :verify-guards nil
    :returns (upd-fr1 frame1-p)
    (b*
        ((iteration (nfix iteration))
         (env1 (envir1-fix env1))
         (fr1 (frame1-fix fr1))        
         ((when (zp iteration)) fr1)
         (fr1 (rep1 (- iteration 1) env1 fr1))
         ((when (frame1->loop-break fr1)) fr1)
         (fr1
             (change-frame1
                 fr1
                 :k
                 (nth
                      (-
                          (+
                              iteration
                              (envir1->lower-bound env1)
                          )
                          1
                      )
                      (frame1->a fr1)
                 )
             )
         )
         (fr1
             (change-frame1
                 fr1
                 :j
                 (-
                     (-
                         (+
                             iteration
                             (envir1->lower-bound env1)
                         )
                         1
                     )
                     1
                 )
             )
         )
         (fr2
             (rep2
                 (+ (frame1->j fr1) 1)
                 (envir2-init
                      (+ (frame1->j fr1) 1)
                      (frame1->k fr1)
                 )
                 (frame2-init
                      (frame1->j fr1)
                      (frame1->a fr1)
                 )
             )
         )
         (fr1
              (change-frame1
                 fr1
                 :j
                 (frame2->j fr2)
              )
         )
         (fr1
              (change-frame1
                 fr1
                 :a
                 (frame2->a fr2)
              )
         )
         (fr1
             (change-frame1
                 fr1
                 :a
                 (update-nth
                     (+ (frame1->j fr1) 1)
                     (frame1->k fr1)
                     (frame1->a fr1)
                 )
             )
         )
         (fr1
             (change-frame1 fr1
                 :i
                 (+
                     iteration
                     (envir1->lower-bound env1)
                 )
             )
         )
        )
    fr1)
    ///
    (defrule rep1-lemma-1
        (equal
            (envir1-fix (envir1-fix env1))
            (envir1-fix env1)
        )
    )
    (defrule rep1-lemma-2
        (equal
            (frame1-fix (frame1-fix fr1))
            (frame1-fix fr1)
        )
    )
    (defrule rep1-lemma-3
        (equal
            (rep1 iteration (envir1-fix env1) fr1)
            (rep1 iteration env1 fr1)
        )
        :hints (("Goal"
                 :expand
                 ((rep1 iteration (envir1-fix env1) fr1)
                 (rep2 iteration env1 fr1))))
        :use rep1-lemma-1
        :do-not-induct t
    )
    (defrule rep1-lemma-4
        (equal
            (rep1 iteration env1 (frame1-fix fr1))
            (rep1 iteration env1 fr1)
        )
        :hints (("Goal"
                 :expand
                 ((rep1 iteration env1 (frame1-fix fr1))
                 (rep1 iteration env1 fr1))))
        :use rep1-lemma-2
        :do-not-induct t
    )
    (defrule rep1-lemma-5
        (integer-listp (frame1->a (rep1 iteration env1 fr1)))
        :induct (dec-induct iteration) 
    )
    (defrule rep1-lemma-6
        (implies
            (integerp iteration)
            (equal
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                (envir2->upper-bound
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                )
            )
        )
        :disable
        (
            rep2-lemma-6
            rep2-lemma-7
            sublist-12
        )
        :enable
        (
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            ifix
            integer-list-fix
        )
        :do-not-induct t
    )
    (defrule rep1-lemma-7
        (implies
            (integerp iteration)
            (equal
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                (frame2->j
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                )
            )
        )
        :disable
        (
            rep2-lemma-6
            rep2-lemma-7
            sublist-12
        )
        :enable
        (
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            ifix
            integer-list-fix
        )
        :do-not-induct t
    )
    (defrule rep1-lemma-8
            (equal
                (frame1->a (rep1 (- iteration 1) env1 fr1))
                (frame2->a
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                )               
            )
        :disable
        (
            rep2-lemma-6
            rep2-lemma-7
            sublist-12
        )
        :enable
        (
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            frame2->a
            ifix
            integer-list-fix
        )
        :do-not-induct t
    )
    (defrule rep1-lemma-9
        (implies
            (and
                (natp iteration)
                (natp (envir1->lower-bound env1))
                (<
                    (-
                        (+
                           iteration
                           (envir1->lower-bound env1)
                        )
                        1
                    )
                    (len (frame1->a fr1))
                )
            )
            (equal
                (len (frame1->a (rep1 iteration env1 fr1)))
                (len (frame1->a fr1))       
            )
        )
        :disable
        (
            rep1-lemma-5
            rep1-lemma-6
            rep1-lemma-7
            rep1-lemma-8
            rep2-lemma-6
            rep2-lemma-7
            rep2-lemma-12
            rep2-lemma-46
            rep2-lemma-89
            sublist-12
            len-of-update-nth
            len-update-nth
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            frame2->a
        )
        :enable
        (
            max
            nfix
            ifix
            integer-list-fix
        )
        :induct (dec-induct iteration)
        :hints (("Subgoal *1/2"
                 :expand ((rep1 iteration env1 fr1)
                          (len
                           (frame2->a
                            (rep2
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                            )
                           )
                          )
                         )
                 :use
                 (
                    rep1-lemma-6
                    rep1-lemma-7
                    rep1-lemma-8
                    (:instance
                         rep2-lemma-6
                         (iteration
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                         )
                         (env2
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                          )     
                     )
                     (:instance
                         rep2-lemma-7
                         (iteration
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                         )
                         (env2
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                          )     
                     )
                     (:instance
                         len-of-update-nth
                         (n
                             (+
                             (frame2->j
                                 (rep2
                                     (+
                                     (-
                                         (-
                                             (+
                                                 iteration
                                                 (envir1->lower-bound env1)
                                             )
                                             1
                                         )
                                         1
                                     )
                                     1
                                     )
                                     (envir2-init
                                         (+
                                             (-
                                                 (-
                                                     (+
                                                         iteration
                                                         (envir1->lower-bound env1)
                                                     )
                                                     1
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (nth
                                             (-
                                                 (+
                                                    iteration
                                                    (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             (frame1->a (rep1 (- iteration 1) env1 fr1))
                                         )
                                     )
                                     (frame2-init
                                         (-
                                             (-
                                                 (+
                                                     iteration
                                                     (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (frame1->a (rep1 (- iteration 1) env1 fr1))
                                     )
                                 )
                             )
                             1
                             )
                         )
                         (v
                             (nth
                                 (-
                                     (+
                                         iteration
                                         (envir1->lower-bound env1)
                                     )
                                     1
                                 )
                                 (frame1->a (rep1 (- iteration 1) env1 fr1))
                             )
                         )
                         (x
                             (frame2->a
                                (rep2
                                     (+
                                     (-
                                         (-
                                             (+
                                                 iteration
                                                 (envir1->lower-bound env1)
                                             )
                                             1
                                         )
                                         1
                                     )
                                     1
                                     )
                                     (envir2-init
                                         (+
                                             (-
                                                 (-
                                                     (+
                                                         iteration
                                                         (envir1->lower-bound env1)
                                                     )
                                                     1
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (nth
                                             (-
                                                 (+
                                                    iteration
                                                    (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             (frame1->a (rep1 (- iteration 1) env1 fr1))
                                         )
                                     )
                                     (frame2-init
                                         (-
                                             (-
                                                 (+
                                                     iteration
                                                     (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (frame1->a (rep1 (- iteration 1) env1 fr1))
                                     )
                                 )
                             )
                         )
                    )
                    (:instance
                         rep2-lemma-46
                         (iteration
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                         )
                         (env2
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                          )     
                     )
                     (:instance
                         rep1-lemma-5
                         (iteration (- iteration 1))
                         (env1 env1)
                         (fr1 fr1)
                     )
                     (:instance
                         sublist-12
                         (i
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                         )
                         (u
                             (frame1->a (rep1 (- iteration 1) env1 fr1))
                         )
                     )
                 )
                 :do-not-induct t))
    )
    (defrule rep1-lemma-10
        (implies
            (and
                (natp iteration)
                (natp (envir1->lower-bound env1))
                (not (frame1->loop-break fr1))
                (<
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    (len (frame1->a fr1))
                )
            )
            (not
                (frame1->loop-break (rep1 iteration env1 fr1))
            )
        )
        :disable
        (
            rep1-lemma-5
            rep1-lemma-6
            rep1-lemma-7
            rep1-lemma-8
            rep1-lemma-9
            rep2-lemma-6
            rep2-lemma-7
            rep2-lemma-12
            rep2-lemma-46
            rep2-lemma-78
            rep2-lemma-89
            sublist-12
            len-of-update-nth
            len-update-nth
            nth-of-update-nth-diff
            nth-update-nth
            update-nth
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            frame2->a
        )
        :enable
        (
            max
            nfix
            ifix
            integer-list-fix
        )
        :induct (dec-induct iteration)
        :hints (("Subgoal *1/2"
                 :expand ((rep1 iteration env1 fr1))
                 :do-not-induct t))
    )
    (defrule rep1-lemma-11
        (implies
            (and
                (natp iteration)
                (natp index)
                (< 0 iteration)
                (not (frame1->loop-break fr1))
                (natp (envir1->lower-bound env1))
                (<=
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    index
                )
                (< index (len (frame1->a fr1)))
            )
            (equal
                (nth
                    index
                    (frame1->a (rep1 iteration env1 fr1))
                )
                (nth
                    index
                    (frame1->a (rep1 (- iteration 1) env1 fr1))
                )
            )
        )
        :disable
        (
            rep1-lemma-5
            rep1-lemma-6
            rep1-lemma-7
            rep1-lemma-8
            rep1-lemma-9
            rep1-lemma-10
            rep2-lemma-5
            rep2-lemma-6
            rep2-lemma-7
            rep2-lemma-12
            rep2-lemma-46
            rep2-lemma-78
            rep2-lemma-89
            sublist-11
            sublist-12
            len-of-update-nth
            len-update-nth
            nth-of-update-nth-diff
            nth-update-nth
            update-nth
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            frame2->a
        )
        :enable
        (
            max
            nfix
            ifix
            integer-list-fix
            update-nth
            nth
        )
        :hints (("Goal"
                 :expand ((rep1 iteration env1 fr1))
                 :use
                 (
                    rep1-lemma-6
                    rep1-lemma-7
                    rep1-lemma-8
                    (:instance
                         rep1-lemma-9
                         (iteration iteration)
                         (env1 env1)
                         (fr1 fr1)
                    )
                    (:instance
                         rep1-lemma-9
                         (iteration (- iteration 1))
                         (env1 env1)
                         (fr1 fr1)
                    )
                    (:instance
                         rep1-lemma-10
                         (iteration iteration)
                         (env1 env1)
                         (fr1 fr1)
                    )
                    (:instance
                         rep1-lemma-10
                         (iteration (- iteration 1))
                         (env1 env1)
                         (fr1 fr1)
                    )
                    (:instance
                         rep2-lemma-6
                         (iteration
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                         )
                         (env2
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                          )     
                     )
                     (:instance
                         rep2-lemma-7
                         (iteration
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                         )
                         (env2
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                          )     
                     )

                       (:instance
                         nth-of-update-nth-diff
                         (n1
                             index
                         )
                         (n2
                             (+
                             (frame2->j
                                 (rep2
                                     (+
                                     (-
                                         (-
                                             (+
                                                 iteration
                                                 (envir1->lower-bound env1)
                                             )
                                             1
                                         )
                                         1
                                     )
                                     1
                                     )
                                     (envir2-init
                                         (+
                                             (-
                                                 (-
                                                     (+
                                                         iteration
                                                         (envir1->lower-bound env1)
                                                     )
                                                     1
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (nth
                                             (-
                                                 (+
                                                    iteration
                                                    (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             (frame1->a (rep1 (- iteration 1) env1 fr1))
                                         )
                                     )
                                     (frame2-init
                                         (-
                                             (-
                                                 (+
                                                     iteration
                                                     (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (frame1->a (rep1 (- iteration 1) env1 fr1))
                                     )
                                 )
                             )
                             1
                             )
                         )
                         (v
                             (nth
                                 (-
                                     (+
                                         iteration
                                         (envir1->lower-bound env1)
                                     )
                                     1
                                 )
                                 (frame1->a (rep1 (- iteration 1) env1 fr1))
                             )
                         )
                         (x
                             (frame2->a
                                (rep2
                                     (+
                                     (-
                                         (-
                                             (+
                                                 iteration
                                                 (envir1->lower-bound env1)
                                             )
                                             1
                                         )
                                         1
                                     )
                                     1
                                     )
                                     (envir2-init
                                         (+
                                             (-
                                                 (-
                                                     (+
                                                         iteration
                                                         (envir1->lower-bound env1)
                                                     )
                                                     1
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (nth
                                             (-
                                                 (+
                                                    iteration
                                                    (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             (frame1->a (rep1 (- iteration 1) env1 fr1))
                                         )
                                     )
                                     (frame2-init
                                         (-
                                             (-
                                                 (+
                                                     iteration
                                                     (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (frame1->a (rep1 (- iteration 1) env1 fr1))
                                     )
                                 )
                             )
                         )
                    )
                    (:instance
                         rep2-lemma-46
                         (iteration
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                         )
                         (env2
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                          )     
                     )
                     (:instance
                         rep2-lemma-5
                         (iteration
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                         )
                         (env2
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                          )     
                     )
                     (:instance
                         rep1-lemma-5
                         (iteration (- iteration 1))
                         (env1 env1)
                         (fr1 fr1)
                     )
                     (:instance
                         rep2-lemma-78
                         (iteration
                            (+
                            (-
                                (-
                                    (+
                                        iteration
                                        (envir1->lower-bound env1)
                                    )
                                    1
                                )
                                1
                            )
                            1
                            )
                         )
                         (index
                             index
                         )
                         (env2
                              (envir2-init
                                  (+
                                      (-
                                          (-
                                              (+
                                                  iteration
                                                  (envir1->lower-bound env1)
                                              )
                                              1
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (nth
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      (frame1->a (rep1 (- iteration 1) env1 fr1))
                                  )
                              )
                         )
                         (fr2
                              (frame2-init
                                  (-
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                                      1
                                  )
                                  (frame1->a (rep1 (- iteration 1) env1 fr1))
                              )
                          )     
                     )
                     (:instance
                         sublist-12
                         (i
                                      (-
                                          (+
                                              iteration
                                              (envir1->lower-bound env1)
                                          )
                                          1
                                      )
                         )
                         (u
                             (frame1->a (rep1 (- iteration 1) env1 fr1))
                         )
                     )
                     (:instance
                         sublist-11
                         (i
                             (+
                             (frame2->j
                                 (rep2
                                     (+
                                     (-
                                         (-
                                             (+
                                                 iteration
                                                 (envir1->lower-bound env1)
                                             )
                                             1
                                         )
                                         1
                                     )
                                     1
                                     )
                                     (envir2-init
                                         (+
                                             (-
                                                 (-
                                                     (+
                                                         iteration
                                                         (envir1->lower-bound env1)
                                                     )
                                                     1
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (nth
                                             (-
                                                 (+
                                                    iteration
                                                    (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             (frame1->a (rep1 (- iteration 1) env1 fr1))
                                         )
                                     )
                                     (frame2-init
                                         (-
                                             (-
                                                 (+
                                                     iteration
                                                     (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (frame1->a (rep1 (- iteration 1) env1 fr1))
                                     )
                                 )
                             )
                             1
                             )
                         )
                         (v
                             (nth
                                 (-
                                     (+
                                         iteration
                                         (envir1->lower-bound env1)
                                     )
                                     1
                                 )
                                 (frame1->a (rep1 (- iteration 1) env1 fr1))
                             )
                         )
                         (u
                            (frame2->a
                                (rep2
                                     (+
                                     (-
                                         (-
                                             (+
                                                 iteration
                                                 (envir1->lower-bound env1)
                                             )
                                             1
                                         )
                                         1
                                     )
                                     1
                                     )
                                     (envir2-init
                                         (+
                                             (-
                                                 (-
                                                     (+
                                                         iteration
                                                         (envir1->lower-bound env1)
                                                     )
                                                     1
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (nth
                                             (-
                                                 (+
                                                    iteration
                                                    (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             (frame1->a (rep1 (- iteration 1) env1 fr1))
                                         )
                                     )
                                     (frame2-init
                                         (-
                                             (-
                                                 (+
                                                     iteration
                                                     (envir1->lower-bound env1)
                                                 )
                                                 1
                                             )
                                             1
                                         )
                                         (frame1->a (rep1 (- iteration 1) env1 fr1))
                                     )
                                 )
                             )
                         )
                     )
                 )
                 :do-not-induct t))
    )
    (defrule rep1-lemma-12
        (implies
            (and
                (natp iteration)
                (natp index)
                (not (frame1->loop-break fr1))
                (natp (envir1->lower-bound env1))
                (<=
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    index
                )
                (< index (len (frame1->a fr1)))
            )
            (equal
                (nth
                    index
                    (frame1->a (rep1 iteration env1 fr1))
                )
                (nth
                    index
                    (frame1->a fr1)
                )
            )
        )
        :disable
        (
            rep1-lemma-5
            rep1-lemma-6
            rep1-lemma-7
            rep1-lemma-8
            rep1-lemma-9
            rep1-lemma-10
            rep1-lemma-11
            rep2-lemma-5
            rep2-lemma-6
            rep2-lemma-7
            rep2-lemma-12
            rep2-lemma-46
            rep2-lemma-78
            rep2-lemma-89
            sublist-11
            sublist-12
            len-of-update-nth
            len-update-nth
            nth-of-update-nth-diff
            nth-update-nth
            update-nth
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            frame2->a
        )
        :enable
        (
            max
            nfix
            ifix
            integer-list-fix
            update-nth
            nth
        )
        :induct (dec-induct iteration)
        :hints (("Subgoal *1/2"
                 :expand ((rep1 iteration env1 fr1))
                 :use
                 (
                     rep1-lemma-6
                     rep1-lemma-7
                     rep1-lemma-8
                     rep1-lemma-11
                 )
                 :do-not-induct t))
    )
    (defrule rep1-lemma-13
        (implies
            (and
                (natp iteration)
                (not (frame1->loop-break fr1))
                (natp (envir1->lower-bound env1))
                (<
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    (len (frame1->a fr1))
                )
            )
            (equal
                (nth
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )                   
                    (frame1->a (rep1 iteration env1 fr1))
                )
                (nth
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    (frame1->a fr1)
                )
            )
        )
        :disable
        (
            rep1-lemma-5
            rep1-lemma-6
            rep1-lemma-7
            rep1-lemma-8
            rep1-lemma-9
            rep1-lemma-10
            rep1-lemma-11
            rep1-lemma-12
            rep2-lemma-5
            rep2-lemma-6
            rep2-lemma-7
            rep2-lemma-12
            rep2-lemma-46
            rep2-lemma-78
            rep2-lemma-89
            sublist-11
            sublist-12
            len-of-update-nth
            len-update-nth
            nth-of-update-nth-diff
            nth-update-nth
            update-nth
            max
            nfix
            ifix
            integer-list-fix
            update-nth
            nth
            envir1
            envir1-fix
            envir1-init
            envir1->lower-bound
            frame1
            frame1-fix
            frame1-init
            frame1->i
            frame1->a
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            frame2->a
        )
        :hints (("Goal"
                 :use
                 (
                     (:instance
                         rep1-lemma-12
                         (iteration iteration)
                         (index
                             (+
                                 iteration
                                 (envir1->lower-bound env1)
                             ) 
                         )
                         (env1 env1)
                         (fr1 fr1)
                     )
                 )
                 :do-not-induct t))
    )
    (defrule rep1-lemma-14
        (implies
            (and
                (natp iteration)
                (not (frame1->loop-break fr1))
                (natp (envir1->lower-bound env1))
                (<
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    (len (frame1->a fr1))
                )
            )
            (equal
                (sublist
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )                    
                    (frame1->a (rep1 iteration env1 fr1))
                )
                (sublist
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    (+
                        iteration
                        (envir1->lower-bound env1)
                    )
                    (frame1->a fr1)
                )
            )
        )
        :disable
        (
            rep1-lemma-5
            rep1-lemma-6
            rep1-lemma-7
            rep1-lemma-8
            rep1-lemma-9
            rep1-lemma-10
            rep1-lemma-11
            rep1-lemma-12
            rep1-lemma-13
            rep2-lemma-5
            rep2-lemma-6
            rep2-lemma-7
            rep2-lemma-12
            rep2-lemma-46
            rep2-lemma-78
            rep2-lemma-89
            sublist-9
            sublist-11
            sublist-12
            len-of-update-nth
            len-update-nth
            nth-of-update-nth-diff
            nth-update-nth
            update-nth
            max
            nfix
            ifix
            integer-list-fix
            update-nth
            nth
            envir1
            envir1-fix
            envir1-init
            envir1->lower-bound
            frame1
            frame1-fix
            frame1-init
            frame1->i
            frame1->a
            envir2
            envir2-fix
            envir2-init
            envir2->upper-bound
            frame2
            frame2-fix
            frame2-init
            frame2->j
            frame2->a
        )
        :hints (("Goal"
                 :use
                 (
                     rep1-lemma-9
                     rep1-lemma-13
                     (:instance
                         sublist-9
                         (i
                             (+
                                 iteration
                                 (envir1->lower-bound env1)
                             )
                         )
                         (u (frame1->a (rep1 iteration env1 fr1)))
                     )
                     (:instance
                         sublist-9
                         (i
                             (+
                                 iteration
                                 (envir1->lower-bound env1)
                             )
                         )
                         (u (frame1->a fr1))
                     )
                 )
                 :do-not-induct t))
    )
)
