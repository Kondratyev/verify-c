(in-package "ACL2")

(include-book "textbook/chap11/qsort" :dir :system)

(include-book "std/util/defrule" :dir :system)
(include-book "centaur/fty/top" :dir :system)
(include-book "std/util/bstar" :dir :system)
(include-book "std/lists/top" :dir :system)
(include-book "std/basic/inductions" :dir :system)

(include-book "sublist")

(define ordered
    ((i integerp)
     (j integerp)
     (u integer-listp))
    :verify-guards nil
    :returns (result booleanp)
    (b* ((i (ifix i))
         (j (ifix j))
         (u (integer-list-fix u)))
    (orderedp (sublist i j u)))
    ///
    (fty::deffixequiv ordered))

(defrule ordered-1
    (implies
        (and
            (natp i)
            (integer-listp u)
        )
        (ordered i i u)
    )
    :enable (ordered orderedp)
    :disable (sublist sublist-1 sublist-2
              sublist-3 sublist-5 sublist-6 sublist-7 sublist-9)
    :hints (("Goal"
            :do-not-induct t
            :expand (ordered i i u))
            ("Goal'''"
            :do-not-induct t
            :expand (SUBLIST I I U)))
    :use sublist-9
)

(local
(defrule ordered-lemma-1
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist i j u)
            (append
                (sublist i k u)
                (sublist (+ k 1) j u)
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14)
    :use sublist-14
)
)

(local
(defrule ordered-lemma-2
    (implies
        (and
            (< (- k 1) i)
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist i k u)
            (append
                (sublist i (- k 1) u)
                (sublist k k u)
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1)
    :hints (("Goal" :expand (sublist i (- k 1) u)))
)
)

(local
(defrule ordered-lemma-3
    (implies
        (and
            (<= i (- k 1))
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist i k u)
            (append
                (sublist i (- k 1) u)
                (sublist k k u)
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2)
    :use ((:instance sublist-14
           (k (- k 1)) (j k)))
)
)

(local
(defrule ordered-lemma-4
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist i k u)
            (append
                (sublist i (- k 1) u)
                (sublist k k u)
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3)
    :hints (("Goal" :cases ((<= i (- k 1))))
            ("Subgoal 2" :use ordered-lemma-2)
            ("Subgoal 1" :use ordered-lemma-3))
)
)

(local
(defrule ordered-lemma-5
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist k k u)
            (cons (nth k u) nil)
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4)
    :use ((:instance sublist-9
           (i k)))
)
)

(local
(defrule ordered-lemma-6
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist i k u)
            (append
                (sublist i (- k 1) u)
                (cons (nth k u) nil)
            )
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5)
    :use (ordered-lemma-4
          ordered-lemma-5)
)
)

(local
(defrule ordered-lemma-7
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (last
                (append
                    (sublist i (- k 1) u)
                    (cons (nth k u) nil)
                )
            )
            (cons (nth k u) nil)
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6)
)
)

(local
(defrule ordered-lemma-8
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (car
                (last
                    (sublist i k u)
                )
            )
            (nth k u)
        )
    )
    :do-not-induct t
    :disable (sublist sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7)
    :use (ordered-lemma-6
          ordered-lemma-7)
)
)

(local
(defrule ordered-lemma-9
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (consp (sublist i k u))
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8)
    :hints (("Goal" :expand ((sublist i k u))))
)
)

(local
(defrule ordered-lemma-10
    (implies
        (and
            (< j (+ k 2))
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) j u)
            (append
                (sublist (+ k 1) (+ k 1) u)
                (sublist (+ k 2) j u)
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9)
    :hints (("Goal" :expand (sublist (+ k 2) j u)))
)
)

(local
(defrule ordered-lemma-11
    (implies
        (and
            (<= (+ k 2) j)
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) j u)
            (append
                (sublist (+ k 1) (+ k 1) u)
                (sublist (+ k 2) j u)
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10)
    :use ((:instance sublist-14
           (i (+ k 1)) (k (+ k 1))))
)
)

(local
(defrule ordered-lemma-12
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) j u)
            (append
                (sublist (+ k 1) (+ k 1) u)
                (sublist (+ k 2) j u)
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11)
    :hints (("Goal" :cases ((<= (+ k 2) j)))
            ("Subgoal 2" :use ordered-lemma-10)
            ("Subgoal 1" :use ordered-lemma-11))
)
)

(local
(defrule ordered-lemma-13
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) (+ k 1) u)
            (cons (nth (+ k 1) u) nil)
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11
              ordered-lemma-12)
    :use ((:instance sublist-9
           (i (+ k 1))))
)
)

(local
(defrule ordered-lemma-14
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (sublist (+ k 1) j u)
            (cons
                (nth (+ k 1) u)
                (sublist (+ k 2) j u)
            )
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11
              ordered-lemma-12 ordered-lemma-13)
    :use (ordered-lemma-12
          ordered-lemma-13)
)
)

(local
(defrule ordered-lemma-15
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (equal
            (car
                (sublist (+ k 1) j u)
            )
            (nth (+ k 1) u)
        )
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11
              ordered-lemma-12 ordered-lemma-13
              ordered-lemma-14)
    :use (ordered-lemma-14)
)
)

(local
(defrule ordered-lemma-16
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (true-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
        )
        (consp (sublist (+ k 1) j u))
    )
    :do-not-induct t
    :enable (sublist)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11
              ordered-lemma-12 ordered-lemma-13
              ordered-lemma-14 ordered-lemma-15)
    :hints (("Goal" :expand ((sublist (+ k 1) j u))))
)
)

(defrule ordered-2
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (integer-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
            (ordered i j u)
        )
        (and
            (ordered i k u)
            (ordered (+ k 1) j u)
            (<=
                (nth k u)
                (nth (+ k 1) u)
            )
        )
    )
    :enable (ordered)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11
              ordered-lemma-12 ordered-lemma-13
              ordered-lemma-14 ordered-lemma-15
              ordered-lemma-16 ordered-1)
    :hints (("Goal"
            :do-not-induct t
            :expand
            (
                (ordered i j u)
                (ordered i k u)
                (ordered (+ k 1) j u)
            )
            :use
            (
                ordered-lemma-1
                ordered-lemma-8
                ordered-lemma-9
                ordered-lemma-15
                ordered-lemma-16
                (:instance orderedp-append
                    (x (sublist i k u))
                    (y (sublist (+ k 1) j u))
                )
            )
            ))
)

(defrule ordered-3
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (integer-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
            (ordered i k u)
            (ordered (+ k 1) j u)
            (<=
                (nth k u)
                (nth (+ k 1) u)
            )    
        )
        (ordered i j u)
    )
    :enable (ordered)
    :disable (sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11
              ordered-lemma-12 ordered-lemma-13
              ordered-lemma-14 ordered-lemma-15
              ordered-lemma-16 ordered-1 ordered-2)
    :hints (("Goal"
            :do-not-induct t
            :expand
            (
                (ordered i j u)
                (ordered i k u)
                (ordered (+ k 1) j u)
            )
            :use
            (
                ordered-lemma-1
                ordered-lemma-8
                ordered-lemma-9
                ordered-lemma-15
                ordered-lemma-16
                (:instance orderedp-append
                    (x (sublist i k u))
                    (y (sublist (+ k 1) j u))
                )
            )
            ))
)

(defrule ordered-4
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (integer-listp u)
            (<= i k)
            (< k j)
            (< j (len u))
            (ordered i j u)
        )
        (ordered i k u)
    )
    :disable (ordered sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11
              ordered-lemma-12 ordered-lemma-13
              ordered-lemma-14 ordered-lemma-15
              ordered-lemma-16 ordered-1 ordered-2
              ordered-3)
    :use (ordered-2)
)

(defrule ordered-5
    (implies
        (and
            (natp i)
            (natp j)
            (natp k)
            (integer-listp u)
            (< i k)
            (<= k j)
            (< j (len u))
            (ordered i j u)
        )
        (ordered k j u)
    )
    :disable (ordered sublist-1 sublist-2 sublist-13
              sublist-14 ordered-lemma-1 ordered-lemma-2
              ordered-lemma-3 ordered-lemma-4
              ordered-lemma-5 ordered-lemma-6
              ordered-lemma-7 ordered-lemma-8
              ordered-lemma-8 ordered-lemma-9
              ordered-lemma-10 ordered-lemma-11
              ordered-lemma-12 ordered-lemma-13
              ordered-lemma-14 ordered-lemma-15
              ordered-lemma-16 ordered-1 ordered-2
              ordered-3 ordered-4)
    :use (:instance ordered-2 (k (- k 1)))
)
